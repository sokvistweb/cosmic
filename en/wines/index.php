<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" lang="en"><!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Còsmic Vinyaters - The Wines</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="Còsmic vinyaters is a winery that produces natural wines and organic wines. We develop cultural wines to give pleasure in the world." />
    <meta name="keywords" content="wine, cellar, ecological" />
    <meta name="author" content="Sokvist" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="../../favicon.ico" />
    <link rel="apple-touch-icon" href="../../apple-touch-icon.png" /><!-- 57×57px -->
    <link rel="apple-touch-icon-precomposed" href="../../apple-touch-icon-precomposed.png"><!-- 180×180px -->
    <link rel="stylesheet" href="../../assets/css/style.css">
	<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,600,900' rel='stylesheet' type='text/css'>
    <link rel="canonical" href="http://cosmic.cat/">
    <link hreflang="ca" href="http://cosmic.cat/" rel="alternate">
    <link hreflang="es" href="http://cosmic.cat/es/" rel="alternate">

    <script src="../../assets/js/vendor/modernizr.js"></script>
  	
</head>
<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status" class="spinner">
            <div id="loading">
            <div id="loading-center">
            <div id="loading-center-absolute">
            <div class="object" id="object_one"></div>
            <div class="object" id="object_two"></div>
            <div class="object" id="object_three"></div>
            <div class="object" id="object_four"></div>
            <div class="object" id="object_five"></div>
            <div class="object" id="object_six"></div>
            <div class="object" id="object_seven"></div>
            <div class="object" id="object_eight"></div>
            <div class="object" id="object_big"></div>
            </div>
            </div>
            </div>
        </div>
    </div> <!-- /#preloader -->

	<header class="cd-header">
		<div id="cd-logo"><a href="../" rel="home" title="Còsmic Vinyaters"><img src="../../assets/img/cosmic-logo.svg" alt="Còsmic"></a></div>

		<nav class="cd-main-nav">
			<ul class="menu" id="nav">
                <li class="hidden"><a href="../#la-vinya">Vineyards</a></li>
				<li><a href="../#el-celler">The Cellar</a></li>
				<li><a href="../#visites">Visits</a></li>
                <li><a href="../#punts-de-venda">Outlets</a></li>
				<li class="hidden"><a href="../#contacte">Contact</a></li>
                <li class="active"><a href="/en/wines/">The Wines</a></li>
			</ul>
		</nav> <!-- cd-main-nav -->
        
        <ul class="language-chooser">
            <li class="lang-ca"><a href="/els-vins/" hreflang="ca" title="Català"><span>ca</span></a></li>
            <li class="lang-es"><a href="/es/los-vinos/" hreflang="es" title="Español"><span>es</span></a></li>
        </ul>
	</header>

	<main class="cd-main-content">
		<div class="cd-fixed-bg cd-bg-3 is-page" id="els-vins">
			<h2>The Wines</h2>
		</div> <!-- cd-fixed-bg -->

		<div class="cd-scrolling-bg cd-color-1 vins">
			<div class="cd-container">
				
                <section class="main work-gridder clearfix">
                    <ul class="gridder">

                        <?php include ("../../includes/gridder-en.php"); ?>

                    </ul>
                </section><!-- end main -->
                
                
                <section class="main clearfix">
                    
                    <?php include ("../../includes/gridder-content-en.php"); ?>
                    
                </section>
                
                
			</div> <!-- cd-container -->
		</div> <!-- cd-scrolling-bg -->

        
        <div class="cd-fixed-bg cd-bg-6" id="contacte">
			<h2>Contact</h2>
		</div> <!-- cd-fixed-bg -->
        
        <div class="cd-scrolling-bg cd-color-1">
			<div class="cd-container">
                <dl>
                    <dt>Celler Còsmic Vinyaters</dt>
                    <dd>Plaça Maria Teresa Pallejà, 3</dd>
                    <dd>17707 Agullana (Alt Empordà)</dd>
                    <dd>Tel. <a href="tel:0034639338176">+ 34 639 338 176</a></dd>
                    <dd><a href="mailto:info@cosmic.cat">info@cosmic.cat</a></dd>
                </dl>
                
                <p class="location"><a href="https://www.google.es/maps/place/Pla%C3%A7a+Maria+Teresa+de+Palleja,+3,+17707+Agullana,+Girona/@42.39429,2.845546,17z/data=!3m1!4b1!4m2!3m1!1s0x12ba9983354ad99d:0xf5cfb6da06874e1a" title="Obre la pàgina de Google Maps" target="_blank">View on Google Maps</a></p>
                
                <ul class="social">
                    <li><a class="youtube" href="https://youtu.be/Lqs9nZN1gvs" title="Còsmic on YouTube" target="_blank"></a></li>
                    <li><a class="facebook" href="https://www.facebook.com/cosmicceller.cosmicceller" title="Our Facebook page" target="_blank"></a></li>
                    <li><a class="twitter" href="https://twitter.com/CosmicCeller" title="Visit us on Twitter" target="_blank"></a></li>
                    <li><a class="insta" href="https://www.instagram.com/cosmicceller/" title="Visit us on Instagram" target="_blank"></a></li>
                </ul>

			</div> <!-- cd-container -->
		</div> <!-- cd-scrolling-bg -->
                        
        <a href="#0" class="cd-top">Top</a>
                        
        <div class="footer">
            <div class="container">
                <span>© Còsmic Vinyaters</span> - <span><a href="http://sokvist.com" title="Sokvist, Web &amp; SEO">Sokvist, Web &amp; SEO</a></span>
            </div>
        </div>
    
    </main> <!-- cd-main-content -->
                        
    
    <script src="../../assets/js/vendor/jquery-1.11.0.min.js"></script>
    <script src="../../assets/js/js/plugins.min.js"></script>
    <script src="../../assets/js/js/main.min.js"></script>
                        
    <!-- Preloader -->
    <script type="text/javascript">
        //<![CDATA[
		$(window).load(function() { // makes sure the whole site is loaded
			$('#status').fadeOut(); // will first fade out the loading animation
			$('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
		})
		//]]>
    </script>
    
</body>
</html>