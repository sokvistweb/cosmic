<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" lang="en"><!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Còsmic Vinyaters</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="Còsmic vinyaters is a winery that produces natural wines and organic wines. We develop cultural wines to give pleasure in the world." />
    <meta name="keywords" content="wine, cellar, ecological" />
    <meta name="author" content="Sokvist" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="../favicon.ico" />
    <link rel="apple-touch-icon" href="../apple-touch-icon.png" /><!-- 57×57px -->
    <link rel="apple-touch-icon-precomposed" href="../apple-touch-icon-precomposed.png"><!-- 180×180px -->
    <link rel="stylesheet" href="../assets/css/style.css">
	<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,600,900' rel='stylesheet' type='text/css'>
    <link rel="canonical" href="http://cosmic.cat/">
    <link hreflang="ca" href="http://cosmic.cat/" rel="alternate">
    <link hreflang="es" href="http://cosmic.cat/es/" rel="alternate">

    <script src="../assets/js/vendor/modernizr.js"></script>
  	
</head>
<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status" class="spinner">
            <div id="loading">
            <div id="loading-center">
            <div id="loading-center-absolute">
            <div class="object" id="object_one"></div>
            <div class="object" id="object_two"></div>
            <div class="object" id="object_three"></div>
            <div class="object" id="object_four"></div>
            <div class="object" id="object_five"></div>
            <div class="object" id="object_six"></div>
            <div class="object" id="object_seven"></div>
            <div class="object" id="object_eight"></div>
            <div class="object" id="object_big"></div>
            </div>
            </div>
            </div>
        </div>
    </div> <!-- /#preloader -->

	<header class="cd-header">
		<div id="cd-logo"><a href="../en/" rel="home" title="Còsmic Vinyaters"><img src="../assets/img/cosmic-logo.svg" alt="Còsmic"></a></div>

		<nav class="cd-main-nav">
			<ul class="menu" id="nav">
                <li class="hidden"><a href="#la-vinya">Vineyards</a></li>
				<li><a href="#el-celler">The Cellar</a></li>
				<li><a href="#visites">Visits</a></li>
                <li><a href="#punts-de-venda">Outlets</a></li>
				<li class="hidden"><a href="#contacte">Contact</a></li>
                <li><a href="/en/wines/">The Wines</a></li>
			</ul>
		</nav> <!-- cd-main-nav -->
        
        <ul class="language-chooser">
            <li class="lang-ca"><a href="../" hreflang="ca" title="Català"><span>ca</span></a></li>
            <li class="lang-es"><a href="../es" hreflang="es" title="Español"><span>es</span></a></li>
        </ul>
	</header>

	<main class="cd-main-content">
		<div class="cd-fixed-bg cd-bg-1-<?php echo(rand(1,3)); ?>" id="la-vinya">
			<h1><img src="../assets/img/cosmic-vinyaters.svg" alt="Còsmic vinyaters"></h1>
		</div> <!-- cd-fixed-bg -->

		<div class="cd-scrolling-bg cd-color-2 with-img">
			<div class="cd-container">
                <div class="spotlight">
                    <div class="slider image">
                        <?php include ("../includes/slider-lavinya.php"); ?>
                    </div>
                    <div class="content copy-right">
                        <p>Cosmic winemakers is a <strong>philosophy of taking care of vines and making wine</strong>, based on what we feel, the feeling of what we are, the passion of what we do, the essence of searching, the patience of finding with the fulfilment of receiving, for having a freer spirit and awareness of a more creative way as winemakers.</p>
                        
                        <p>Còsmic's gaze focuses on <strong>two very different landscapes of the Catalan geography</strong>, but very much ours: <strong>Agullana, Capmany and La Vajol</strong> in the Alt Empordà, where we worship local Empordà varieties, and <strong>Rodonyà and the Sierra de Montmell</strong> in Baix Penedès , where we cultivate autochthonous penedesencas varieties together with foreign varieties well adapted to our terroir.</p>
                        
                        <div class="arrow bounce">
                            <a href="#content-2" class="more"><span>Read more</span></a>
                        </div>
                    </div>
                </div>
			</div> <!-- cd-container -->
		</div> <!-- cd-scrolling-bg -->

		<div class="cd-fixed-bg cd-bg-2" id="el-celler">
			<!--<h2>El Celler</h2>-->
		</div> <!-- cd-fixed-bg -->

		<div class="cd-scrolling-bg cd-color-3 with-img" id="content-2">
			<div class="cd-container">
                <div class="spotlight reverse">
                    <div class="slider image">
                        <?php include ("../includes/slider-elceller.php"); ?>
                    </div>
                    <div class="content copy-left">
                        <p>Two landscapes connected in one focus that decodes terroirs, loves grapes, interprets vines, touches the earth and manages 9.5 ha of <strong>vineyards cultivated in a natural, ecological and biodynamical way</strong>.</p>
                        
                        <p>Our own creative path is devoted to producing <strong>pure integral wines</strong>, made only from one and only grape variety, (pureness upon pureness), with the lowest intervention and the highest respect for its natural  and energetic origin, for the climate and the land, together with the spirit of the person behind it, who uses <strong>energetic cleasing, sacred geometry and sound vibrations</strong> to achieve harmonization and comprehension between the people and the wines.</p>
                    </div>
                </div>
			</div> <!-- cd-container -->
		</div> <!-- cd-scrolling-bg -->


		<div class="cd-fixed-bg cd-bg-4" id="visites">
			<h2>Visits</h2>
		</div> <!-- cd-fixed-bg -->
        
        <div class="cd-scrolling-bg cd-color-2 with-img">
			<div class="cd-container">
                <div class="spotlight">
                    <div class="slider image">
                        <?php include ("../includes/slider-visites.php"); ?>
                    </div>
                    <div class="content">
                        <p>Depending on the season, guided tours are organized in our vineyards with final tasting at the winery for groups of at least 6 people and maximum of 10 people, always by reservation at <a href="mailto:info@cosmic.cat">info@cosmic.cat</a>.</p>
                        
                        <p>The approximate duration of the tours is between 1.5 and 2 hours. During the tour the vineyards and the working philosophy of the winery is known, and the tour ends with a final tasting of all the wines we produce.</p>
                        
                        <p>The cost of the tour is 15 euros per person.</p>
                    </div>
                </div>
			</div> <!-- cd-container -->
		</div> <!-- cd-scrolling-bg -->
        
        <div class="cd-fixed-bg cd-bg-5" id="punts-de-venda">
			<h2>Outlets</h2>
		</div> <!-- cd-fixed-bg -->
        
        <div class="cd-scrolling-bg cd-color-3">
			<div class="cd-container">
				<p>You can buy our wines by going to any of our provincial distributors. We also do direct sales from the warehouse and ship to anywhere in the Iberian Peninsula and the Balearic Islands. Just say what you're interested in <a href="mailto:info@cosmic.cat">info@cosmic.cat</a> and we will make a budget with shipping to your door; so quick and easy.</p>
                
                <h3>Catalonia</h3>
                
                <h4>Girona:</h4>
                <ul>
                    <li>Jaume Jordà, <a href="tel:0034629766722">629 766 722</a> - <a href="mailto:jaume@jaumejorda.com">jaume@jaumejorda.com</a></li>
                    <li>Vins Estela, <a href="tel:0034649457554">649 457 554</a> - <a href="mailto:estela@vinsestela.com">estela@vinsestela.com</a></li>
                </ul>
                
                <h4>Barcelona:</h4>
                <ul>
                    <li>Vilaviniteca, <a href="tel:902327777">902 327 777</a> - <a href="mailto:info@vilaviniteca.es">info@vilaviniteca.es</a></li>
                    <li>Vic: Jaume Jordà, <a href="tel:0034629766722">629 766 722</a> - <a href="mailto:jaume@jaumejorda.com">jaume@jaumejorda.com</a></li>
                </ul>
                
                <h4>Tarragona:</h4>
                <ul>
                    <li>Bonbocam, <a href="tel:0034675900215">675 900 215</a> - <a href="mailto:bonbocam@bonbocam.com">bonbocam@bonbocam.com</a></li>
                </ul>
                
                <h3>Spain</h3>
                <ul>
                    <li>Whole territory: A la Volé, <a href="tel:0034608154080">608 154 080</a> - <a href="info@alavole.com">info@alavole.com</a></li>
                    <li>Eivissa: Supernaturalwines, <a href="tel:0034607735371">607 735 371</a> - <a href="mailto:bernattatjer@me.com">bernattatjer@me.com</a></li>
                    <li>Alacant: Winemultiverse, <a href="tel:0034690630245">690 630 245</a> - <a href="mailto:winemultiverse@gmail.com">winemultiverse@gmail.com</a></li>
                </ul>
                
                <h3>Internacional</h3>
                <p>Refer directly to the winery: <a href="tel:0034639338176">+34 639 338 176</a> - <a href="mailto:info@cosmic.cat">info@cosmic.cat</a></p>
                
			</div> <!-- cd-container -->
		</div> <!-- cd-scrolling-bg -->
        
        <div class="cd-fixed-bg cd-bg-6" id="contacte">
			<h2>Contact</h2>
		</div> <!-- cd-fixed-bg -->
        
        <div class="cd-scrolling-bg cd-color-1">
			<div class="cd-container">
                <dl>
                    <dt>Celler Còsmic Vinyaters</dt>
                    <dd>Plaça Maria Teresa Pallejà, 3</dd>
                    <dd>17707 Agullana (Alt Empordà)</dd>
                    <dd>Tel. <a href="tel:0034639338176">+ 34 639 338 176</a></dd>
                    <dd><a href="mailto:info@cosmic.cat">info@cosmic.cat</a></dd>
                </dl>
                
                <p class="location"><a href="https://www.google.es/maps/place/Pla%C3%A7a+Maria+Teresa+de+Palleja,+3,+17707+Agullana,+Girona/@42.39429,2.845546,17z/data=!3m1!4b1!4m2!3m1!1s0x12ba9983354ad99d:0xf5cfb6da06874e1a" title="Obre la pàgina de Google Maps" target="_blank">View on Google Maps</a></p>
                
                <ul class="social">
                    <li><a class="youtube" href="https://youtu.be/Lqs9nZN1gvs" title="Còsmic on YouTube" target="_blank"></a></li>
                    <li><a class="facebook" href="https://www.facebook.com/cosmicceller.cosmicceller" title="Our Facebook page" target="_blank"></a></li>
                    <li><a class="twitter" href="https://twitter.com/CosmicCeller" title="Visit us on Twitter" target="_blank"></a></li>
                    <li><a class="insta" href="https://www.instagram.com/cosmicceller/" title="Visit us on Instagram" target="_blank"></a></li>
                </ul>

			</div> <!-- cd-container -->
		</div> <!-- cd-scrolling-bg -->
                        
        <a href="#0" class="cd-top">Top</a>
                        
        <div class="footer">
            <div class="container">
                <span>© Còsmic Vinyaters</span> - <span><a href="http://sokvist.com" title="Sokvist, Web &amp; SEO">Sokvist, Web &amp; SEO</a></span>
            </div>
        </div>
    
    </main> <!-- cd-main-content -->
                        
    
    <script src="../assets/js/vendor/jquery-1.11.0.min.js"></script>
    <script src="../assets/js/js/plugins.min.js"></script>
    <script src="../assets/js/js/main.min.js"></script>
                        
    <!-- Preloader -->
    <script type="text/javascript">
        //<![CDATA[
		$(window).load(function() { // makes sure the whole site is loaded
			$('#status').fadeOut(); // will first fade out the loading animation
			$('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
		})
		//]]>
    </script>
    
    <!--<script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-47544981-1', 'sokvist.com');
        ga('send', 'pageview');
    </script>-->
    
</body>
</html>