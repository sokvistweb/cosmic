jQuery(document).ready(function($){
	
    
    //open-close submenu on mobile
	$('.cd-main-nav').on('click', function(event){
		if($(event.target).is('.cd-main-nav')) $(this).children('ul').toggleClass('is-visible');
	});
    
    
    
    // One Page Nav
    $('#nav').onePageNav({
		currentClass: 'current',
		changeHash: true,
		scrollSpeed: 1500,
		scrollThreshold: 0.5,
		filter: '',
		easing: 'easeInOutExpo'
	});
    
    
    
    // Smooth scrolling
    $('a.more').click(function() {
		$('html, body').animate({
			scrollTop: $($(this).attr('href')).offset().top - 72
		}, {
			duration: 2000,
			easing: 'swing'
		});
		return false;
	});
    
    
    
    // sss Slider
    $('.slider').sss({
        slideShow : true, // Set to false to prevent SSS from automatically animating.
        startOn : 0, // Slide to display first. Uses array notation (0 = first slide).
        transition : 1000, // Length (in milliseconds) of the fade transition.
        speed : 4000, // Slideshow speed in milliseconds.
        showNav : true // Set to false to hide navigation arrows.
    });
    
    
    
    // Call Gridder
    $(".gridder").gridderExpander({
        scrollOffset: 140,
        scrollTo: "panel", // "panel" or "listitem"
        animationSpeed: 500,
        animationEasing: "easeInOutExpo",
        onStart: function () {
            console.log("Gridder Inititialized");
        },
        onExpanded: function (object) {
            console.log("Gridder Expanded");
            $(".carousel").carousel();
        },
        onChanged: function (object) {
            console.log("Gridder Changed");
        },
        onClosed: function () {
            console.log("Gridder Closed");
        }
    });
    
    
    // Back to top
    // browser window scroll (in pixels) after which the "back to top" link is shown
	var offset = 300,
		//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
		offset_opacity = 1200,
		//duration of the top scrolling animation (in ms)
		scroll_top_duration = 1500,
		//grab the "back to top" link
		$back_to_top = $('.cd-top');

	//hide or show the "back to top" link
	$(window).scroll(function(){
		( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
		if( $(this).scrollTop() > offset_opacity ) { 
			$back_to_top.addClass('cd-fade-out');
		}
	});

	//smooth scroll to top
	$back_to_top.on('click', function(event){
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0,
		 	}, scroll_top_duration
		);
	});
    
});
