<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" lang="es"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" lang="es"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" lang="es"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" lang="es"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" lang="es"><!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Còsmic Vinyaters</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="Còsmic vinyaters es una micro bodega que elabora vinos naturales y vinos ecológicos. Hacemos vinos culturales para dar placer en el mundo." />
    <meta name="keywords" content="vinos, bodega, ecológico" />
    <meta name="author" content="Sokvist" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="../favicon.ico" />
    <link rel="apple-touch-icon" href="../apple-touch-icon.png" /><!-- 57×57px -->
    <link rel="apple-touch-icon-precomposed" href="../apple-touch-icon-precomposed.png"><!-- 180×180px -->
    <link rel="stylesheet" href="../assets/css/style.css">
	<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,600,900' rel='stylesheet' type='text/css'>
    <link rel="canonical" href="http://cosmic.cat/">
    <link hreflang="ca" href="http://cosmic.cat/" rel="alternate">
    <link hreflang="en" href="http://cosmic.cat/en/" rel="alternate">

    <script src="../assets/js/vendor/modernizr.js"></script>
  	
</head>
<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status" class="spinner">
            <div id="loading">
            <div id="loading-center">
            <div id="loading-center-absolute">
            <div class="object" id="object_one"></div>
            <div class="object" id="object_two"></div>
            <div class="object" id="object_three"></div>
            <div class="object" id="object_four"></div>
            <div class="object" id="object_five"></div>
            <div class="object" id="object_six"></div>
            <div class="object" id="object_seven"></div>
            <div class="object" id="object_eight"></div>
            <div class="object" id="object_big"></div>
            </div>
            </div>
            </div>
        </div>
    </div> <!-- /#preloader -->

	<header class="cd-header">
		<div id="cd-logo"><a href="../" rel="home" title="Còsmic Vinyaters"><img src="../assets/img/cosmic-logo.svg" alt="Còsmic"></a></div>

		<nav class="cd-main-nav">
			<ul class="menu" id="nav">
                <li class="hidden"><a href="#la-vinya">La viña</a></li>
				<li><a href="#el-celler">La bodega</a></li>
				<li><a href="#visites">Visitas</a></li>
                <li><a href="#punts-de-venda">Puntos de venta</a></li>
				<li class="hidden"><a href="#contacte">Contacto</a></li>
                <li><a href="/es/los-vinos/">Los Vinos</a></li>
			</ul>
		</nav> <!-- cd-main-nav -->
        
        <ul class="language-chooser">
            <li class="lang-ca"><a href="../" hreflang="ca" title="Català"><span>ca</span></a></li>
            <li class="lang-en"><a href="../en" hreflang="en" title="English"><span>en</span></a></li>
        </ul>
	</header>

	<main class="cd-main-content">
		<div class="cd-fixed-bg cd-bg-1-<?php echo(rand(1,3)); ?>" id="la-vinya">
			<h1><img src="../assets/img/cosmic-vinyaters.svg" alt="Còsmic vinyaters"></h1>
		</div> <!-- cd-fixed-bg -->

		<div class="cd-scrolling-bg cd-color-2 with-img">
			<div class="cd-container">
                <div class="spotlight">
                    <div class="slider image">
                        <?php include ("../includes/slider-lavinya.php"); ?>
                    </div>
                    <div class="content copy-right">
                        <p>Còsmic Vinyaters es <strong>una filosofía de cuidar viñas y hacer vino</strong>, basada en los valores de la confianza en lo que sentimos, la valentía de ser quienes somos, la pasión en lo que hacemos, la esencia en el buscar, la paciencia en el encontrar y la gratitud en el recibir, para ser más libres de espíritu y más conscientes en nuestro camino creativo como labradores viticultores que somos.</p>
                        
                        <p>La mirada de Còsmic se centra en <strong>dos paisajes muy diferentes de la geografía catalana</strong>, pero muy nuestros: <strong>Agullana, Capmany y La Vajol</strong> en el Alt Empordà, donde rendimos culto a variedades autóctonas ampurdanesas, y <strong>Rodonyà y la Sierra de Montmell</strong> en el Baix Penedès, donde cultivamos variedades autóctonas penedesencas junto con variedades foráneas bien adaptadas a nuestro terruño.</p>
                        
                        <div class="arrow bounce">
                            <a href="#content-2" class="more"><span>Seguir leyendo</span></a>
                        </div>
                    </div>
                </div>
			</div> <!-- cd-container -->
		</div> <!-- cd-scrolling-bg -->

		<div class="cd-fixed-bg cd-bg-2" id="el-celler">
			<!--<h2>El Celler</h2>-->
		</div> <!-- cd-fixed-bg -->

		<div class="cd-scrolling-bg cd-color-3 with-img" id="content-2">
			<div class="cd-container">
                <div class="spotlight reverse">
                    <div class="slider image">
                        <?php include ("../includes/slider-elceller.php"); ?>
                    </div>
                    <div class="content copy-left">
                        <p>Dos mundos unidos bajo una misma mirada que descifra terroirs, ama las uvas, interpreta las variedades, toca la tierra y actúa trabajando 9,5 hectáreas de viña, cultivadas de la manera más natural posible, practicando una <strong>agricultura ecológica y biodinámica</strong>.</p>
                        
                        <p>Nuestro camino creativo se centra en hacer <strong>vinos integrales y puros</strong>, siempre partiendo de una sola variedad de uva (pureza sobre pureza), con la mínima intervención y el máximo respeto por el origen natural y energético de la uva, la tierra, el clima y el espíritu de la persona que hay detrás, que hace uso de técnicas de <strong>limpieza energética, geometría sagrada y vibraciones sonoras</strong> para la armonización del espacio, las personas y los vinos.</p>
                    </div>
                </div>
			</div> <!-- cd-container -->
		</div> <!-- cd-scrolling-bg -->


		<div class="cd-fixed-bg cd-bg-4" id="visites">
			<h2>Visitas</h2>
		</div> <!-- cd-fixed-bg -->
        
        <div class="cd-scrolling-bg cd-color-2 with-img">
			<div class="cd-container">
                <div class="spotlight">
                    <div class="slider image">
                        <?php include ("../includes/slider-visites.php"); ?>
                    </div>
                    <div class="content">
                        <p>Dependiendo de la época del año, se organizan visitas guiadas en nuestros viñedos con cata final en la bodega para grupos de al menos 6 personas y máximo de 10 personas, siempre con reserva previa en el correo <a href="mailto:info@cosmic.cat">info@cosmic.cat</a>.</p>
                        
                        <p>La duración aproximada de las visitas guiadas es entre 1,5 y 2 horas. Durante el recorrido se conocen las viñas y la filosofía de trabajo de la bodega, y la visita termina con una degustación final de todos los vinos que elaboramos.</p>
                        
                        <p>El coste de la visita es de 15 euros por persona.</p>
                    </div>
                </div>
			</div> <!-- cd-container -->
		</div> <!-- cd-scrolling-bg -->
        
        <div class="cd-fixed-bg cd-bg-5" id="punts-de-venda">
			<h2>Puntos de venta</h2>
		</div> <!-- cd-fixed-bg -->
        
        <div class="cd-scrolling-bg cd-color-3">
			<div class="cd-container">
				<p>Puedes adquirir nuestros vinos dirigiéndote a cualquiera de nuestros distribuidores provinciales. También hacemos venta directa desde la bodega y realizamos envíos a cualquier punto de la Península Ibérica y las islas Baleares. Simplemente di lo que te interesa en <a href="mailto:info@cosmic.cat">info@cosmic.cat</a> y te haremos un presupuesto con los gastos de envío a la puerta de tu casa; así de fácil y rápido.</p>
                
                <h3>Cataluña</h3>
                
                <h4>Girona:</h4>
                <ul>
                    <li>Jaume Jordà, <a href="tel:0034629766722">629 766 722</a> - <a href="mailto:jaume@jaumejorda.com">jaume@jaumejorda.com</a></li>
                    <li>Vins Estela, <a href="tel:0034649457554">649 457 554</a> - <a href="mailto:estela@vinsestela.com">estela@vinsestela.com</a></li>
                </ul>
                
                <h4>Barcelona:</h4>
                <ul>
                    <li>Vilaviniteca, <a href="tel:902327777">902 327 777</a> - <a href="mailto:info@vilaviniteca.es">info@vilaviniteca.es</a></li>
                    <li>Jaume Jordà, <a href="tel:0034629766722">629 766 722</a> - <a href="mailto:jaume@jaumejorda.com">jaume@jaumejorda.com</a></li>
                </ul>
                
                <h4>Tarragona:</h4>
                <ul>
                    <li>Bonbocam, <a href="tel:0034675900215">675 900 215</a> - <a href="mailto:bonbocam@bonbocam.com">bonbocam@bonbocam.com</a></li>
                </ul>
                
                <h3>España</h3>
                <ul>
                    <li>Todo el territorio: A la Volé, <a href="tel:0034608154080">608 154 080</a> - <a href="info@alavole.com">info@alavole.com</a></li>
                    <li>Eivissa: Supernaturalwines, <a href="tel:0034607735371">607 735 371</a> - <a href="mailto:bernattatjer@me.com">bernattatjer@me.com</a></li>
                    <li>Alacant: Winemultiverse, <a href="tel:0034690630245">690 630 245</a> - <a href="mailto:winemultiverse@gmail.com">winemultiverse@gmail.com</a></li>
                </ul>
                
                <h3>Internacional</h3>
                <p>Consulte directamente en la bodega: <a href="tel:0034639338176">+34 639 338 176</a> - <a href="mailto:info@cosmic.cat">info@cosmic.cat</a></p>
                
			</div> <!-- cd-container -->
		</div> <!-- cd-scrolling-bg -->
        
        <div class="cd-fixed-bg cd-bg-6" id="contacte">
			<h2>Contacto</h2>
		</div> <!-- cd-fixed-bg -->
        
        <div class="cd-scrolling-bg cd-color-1">
			<div class="cd-container">
                <dl>
                    <dt>Celler Còsmic Vinyaters</dt>
                    <dd>Plaça Maria Teresa Pallejà, 3</dd>
                    <dd>17707 Agullana (Alt Empordà)</dd>
                    <dd>Tel. <a href="tel:0034639338176">+ 34 639 338 176</a></dd>
                    <dd><a href="mailto:info@cosmic.cat">info@cosmic.cat</a></dd>
                </dl>
                
                <p class="location"><a href="https://www.google.es/maps/place/Pla%C3%A7a+Maria+Teresa+de+Palleja,+3,+17707+Agullana,+Girona/@42.39429,2.845546,17z/data=!3m1!4b1!4m2!3m1!1s0x12ba9983354ad99d:0xf5cfb6da06874e1a" title="Obre la pàgina de Google Maps" target="_blank">Ver en Google Maps</a></p>
                
                <ul class="social">
                    <li><a class="youtube" href="https://youtu.be/Lqs9nZN1gvs" title="Còsmic en YouTube" target="_blank"></a></li>
                    <li><a class="facebook" href="https://www.facebook.com/cosmicceller.cosmicceller" title="NUestra página de Facebook" target="_blank"></a></li>
                    <li><a class="twitter" href="https://twitter.com/CosmicCeller" title="Visítanos en Twitter" target="_blank"></a></li>
                    <li><a class="insta" href="https://www.instagram.com/cosmicceller/" title="Visítanos en Instagram" target="_blank"></a></li>
                </ul>

			</div> <!-- cd-container -->
		</div> <!-- cd-scrolling-bg -->
                        
        <a href="#0" class="cd-top">Top</a>
                        
        <div class="footer">
            <div class="container">
                <span>© Còsmic Vinyaters</span> - <span><a href="http://sokvist.com" title="Sokvist, Web &amp; SEO">Sokvist, Web &amp; SEO</a></span>
            </div>
        </div>
    
    </main> <!-- cd-main-content -->
                        
    
    <script src="../assets/js/vendor/jquery-1.11.0.min.js"></script>
    <script src="../assets/js/js/plugins.min.js"></script>
    <script src="../assets/js/js/main.min.js"></script>
                        
    <!-- Preloader -->
    <script type="text/javascript">
        //<![CDATA[
		$(window).load(function() { // makes sure the whole site is loaded
			$('#status').fadeOut(); // will first fade out the loading animation
			$('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
		})
		//]]>
    </script>
    
    <!--<script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-47544981-1', 'sokvist.com');
        ga('send', 'pageview');
    </script>-->
    
</body>
</html>