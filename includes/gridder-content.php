<div class="gridder-content" id="consciencia-cosmica">
    <div class="gridder-copy">
        <img src="../assets/img/vins-2020/consciencia-cosmica-2020-large.jpg" class="media-v" alt="Consciència Còsmica" width="370" height="900" />

        <h2>Consciència Còsmica</h2>
        <p><strong>Motivació:</strong> Donar culte a la Carinyena gris en estat pur. Iniciar la creació d'un nou terroir d'alçada, al cel de l'Empordà, a 600 metres d'alçada en una zona verge i plena de biodiversitat.</p>
        
        <p><strong>Fitxes tècniques</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../uploads/2021/A4-consciencia-cosmica-2021.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Consciència Còsmica 2021</h3>
            </li>
            <li>
                <a class="pdf-file" href="../uploads/2022/A4-consciencia-cosmica-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Consciència Còsmica 2022</h3>
            </li>
        </ul>
    </div>
</div>

<div class="gridder-content" id="constelacio">
    <div class="gridder-copy">
        <img src="../assets/img/vins-2020/constelacio-2020-large.jpg" class="media-v" alt="Constel·lació" width="370" height="900" />

        <h2>Constel·lació</h2>
        <p><strong>Motivació:</strong> Retre homenatge a totes les varietats autòctones empordaneses recuperades que cultivem en una mateixa parcel·la de nova plantació, construint  un vi amb totes elles juntes, trobant la bellesa de l’equilibri que existeix en la diversitat.</p>
        
        <p><strong>Fitxes tècniques</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../uploads/2021/A4-constelacio-2021.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Constel·lació 2021</h3>
            </li>
            <li>
                <a class="pdf-file" href="../uploads/2022/A4-constelacio-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Constel·lació 2022</h3>
            </li>
        </ul>
    </div>
</div>

<div class="gridder-content" id="el-teu-mar">
    <div class="gridder-copy">
        <img src="../assets/img/vins-2020/el-teu-mar-2020-large.jpg" class="media-v" alt="El teu mar les meves muntanyes" width="370" height="900" />

        <h2>El teu mar les meves muntanyes</h2>
        <p><strong>Motivació:</strong> És un vi que neix de l’amistat. És una abraçada entre les garnatxes negres, blanques i grises de la Yoyo (el mar) de Banyuls, i les carinyenes blanques, grises i negres de Còsmic (les muntanyes) d’Agullana, materialitzant així la nostra amistat unint mar i muntanyes en un vi. </p>
        
        <p><strong>Fitxes tècniques</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../uploads/2021/A4-el-teu-mar-les-meves-muntanyes-2021.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>El teu mar les meves muntanyes 2021</h3>
            </li>
            <li>
                <a class="pdf-file" href="../uploads/2022/A4-el-teu-mar-les-meves-muntanyes-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>El teu mar les meves muntanyes 2022</h3>
            </li>
        </ul>
    </div>
</div>

<div class="gridder-content" id="gratitud-sauvignon-blanc">
    <div class="gridder-copy">
        <img src="../assets/img/vins-2017/gratitud-sauvignon-blanc-2016-large.jpg" class="media-v" alt="Gratitud Sauvignon blanc 2019" width="370" height="900" />

        <h2>Gratitud blanc</h2>
        <p><strong>Motivació:</strong> Gratitud a la terra i les persones amb les que he nascut i he crescut, obertura al Món integrant les varietats internacionals que millor s’adapten a la nostra terra i climatologia.</p>
        
        <p><strong>Fitxes tècniques</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../uploads/2021/A4-gratitud-blanc-2021 .pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Gratitud blanc 2021</h3>
            </li>
            <li>
                <a class="pdf-file" href="../uploads/2022/A4-gratitud-blanc-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Gratitud blanc 2022</h3>
            </li>
        </ul>
    </div>
</div>

<div class="gridder-content" id="gratitud-cabernet-franc">
    <div class="gridder-copy">
        <img src="../assets/img/vins-2017/gratitud-cabernet-franc-2016-large.jpg" class="media-v" alt="Gratitud Cabernet franc 2019" width="370" height="900" />

        <h2>Gratitud negre</h2>
        
        <p><strong>Motivació:</strong> Gratitud a la terra i les persones amb les que he nascut i he crescut, obertura al Món integrant  les varietats internacionals que millor s’adapten a la nostra terra i climatologia.</p>

        <p><strong>Fitxes tècniques</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../uploads/gratitud-cabernet-franc-2019-ca.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Gratitud Cabernet franc 2019</h3>
            </li>
            <li>
                <a class="pdf-file" href="../uploads/2020/a4-gratitud-cabernet-franc-2020.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Gratitud Cabernet franc 2020</h3>
            </li>
        </ul>
    </div>
</div>

<div class="gridder-content" id="confianca-garnatxa-roja">
    <div class="gridder-copy">
        <img src="../assets/img/vins-2019/confiansa-2019-large.jpg" class="media-v" alt="Confiança Garnatxa roja 2019" width="370" height="900" />

        <h2>Confiança</h2>
        
        <p><strong>Motivació: </strong>retre culte a la Garnatxa Roja, el primer pas en el camí estacional de fer vi, la primera interpretació de tot un any de treball a la terra abraçant el valor de la confiança, i saltar al buit del desconegut any rere any per a seguir volant en el seu cel, on el color és el rosa del sol que es pon per a tornar a néixer.</p>
        
        <p><strong>Fitxes tècniques</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../uploads/2021/A4-confiansa-2021.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Confiança 2021</h3>
            </li>
            <li>
                <a class="pdf-file" href="../uploads/2022/A4-confiansa-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Confiança 2022</h3>
            </li>
        </ul>

    </div>
</div>

<div class="gridder-content" id="valentia-carinyena-blanca">
    <div class="gridder-copy">
        <img src="../assets/img/vins-2019/valentia-2019-large.jpg" class="media-v" alt="Valentia Carinyena blanca 2019" width="370" height="900" />

        <h2>Valentia</h2>

        <p><strong>Motivació:</strong> Retre culte a la carinyena blanca, sobreposar l’instint i la intuïció amb la major força i valor davant els condicionaments establerts que ens envolten i que ens limiten, per poder anar endins, en profunditat, cap allò que estem destinats a ser, la nostra identitat real, la connexió amb la nostra terra i el nostre ser, sense pors.</p>
        
        <p><strong>Fitxes tècniques</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../uploads/2021/A4-valentia-2021.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Valentia 2021</h3>
            </li>
            <li>
                <a class="pdf-file" href="../uploads/2022/A4-valentia-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Valentia 2022</h3>
            </li>
        </ul>
        
    </div>
</div>

<div class="gridder-content" id="via-fora-macabeu">
    <div class="gridder-copy">
        <img src="../assets/img/vins-2019/via-fora-macabeu-2019-large.jpg" class="media-v" alt="Via Fora Macabeu 2019" width="370" height="900" />

        <h2>Via Fora blanc</h2>
        
        <p><strong>Motivació:</strong> Es un vi que neix de la fermesa en les situacions difícils de la vida, on la nostra identitat pot quedar reduïda a l’oblit. És un homenatge als pagesos savis, que han sigut ferms conservadors del nostre patrimoni cultural i vitivinícola arreu del territori del nostre estimat país.</p>
        
        <p><strong>Fitxes tècniques</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../uploads/2021/A4-via-fora-blanc-2021.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Via Fora blanc 2021</h3>
            </li>
            <li>
                <a class="pdf-file" href="../uploads/2022/A4-via-fora-blanc-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Via Fora blanc 2022</h3>
            </li>
        </ul>
        
    </div>
</div>

<div class="gridder-content" id="via-fora-sumoll">
    <div class="gridder-copy">
        <img src="../assets/img/vins-2019/via-fora-sumoll-2019-large.jpg" class="media-v" alt="Via Fora Sumoll 2019" width="370" height="900" />

        <h2>Via Fora negre</h2>
        
        <p><strong>Motivació:</strong> Es un vi que neix de la fermesa en les situacions difícils de la vida, on la nostra identitat pot quedar reduïda a l’oblit. És un homenatge als pagesos savis, que han sigut ferms conservadors del nostre patrimoni cultural i vitivinícola arreu del territori del nostre estimat país.</p>
        
        <p><strong>Fitxes tècniques</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../uploads/via-fora-sumoll-2019-ca.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Via Fora sumoll 2019</h3>
            </li>
            <li>
                <a class="pdf-file" href="../uploads/2020/a4-via-fora-sumoll-2020.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Via Fora sumoll 2020</h3>
            </li>
        </ul>
        
    </div>
</div>

<div class="gridder-content" id="passio-marcelan">
    <div class="gridder-copy">
        <img src="../assets/img/vins-2019/passio-2019-large.jpg" class="media-v" alt="Passió 2019" width="370" height="900" />

        <h2>Passió</h2>
        
        <p><strong>Motivació:</strong> La Passió és el vi que neix fruit del sentiment d’amor a la vinya i a les terres que treballo i que dominen la meva voluntat i raó, és el nexe d’unió entre tots els vins que faig, el valor que fa de motor entre tots ells i també és una abraçada de complicitat entre els dos territoris que interpreto amb passió absoluta, el meus orígens i el meu present.</p>
        
        <p><strong>Fitxes tècniques</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../uploads/2021/A4-passio-2021.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Passió 2021</h3>
            </li>
            <li>
                <a class="pdf-file" href="../uploads/2022/A4-passio-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Passió 2022</h3>
            </li>
        </ul>
        
    </div>
</div>

<div class="gridder-content" id="encarinyades">
    <div class="gridder-copy">
        <img src="../assets/img/vins-2019/encarinyades-2019-large.jpg" class="media-v" alt="Encarinyades 2019" width="370" height="900" />

        <h2>Encarinyades</h2>
        
        <p><strong>Motivació: </strong>Recuperar de nou les genètiques de la carinyena blanca, negra i gris, tot jugant amb elles, reinterpretar la seva màgia, gaudir del seu joc de complicitat, intentar de fer un vi on es mostri la delicadesa i la profunditat d’unes varietats autòctones que són una gran part de l’ADN de l’Empordà, on somio despert.</p>
        
        <p><strong>Fitxes tècniques</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../uploads/2021/A4-encarinyades-2021.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Encarinyades 2021</h3>
            </li>
            <li>
                <a class="pdf-file" href="../uploads/2022/A4-encarinyades-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Encarinyades 2022</h3>
            </li>
        </ul>
        
    </div>
</div>

<div class="gridder-content" id="desti">
    <div class="gridder-copy">
        <img src="../assets/img/vins-2019/desti-2019-large.jpg" class="media-v" alt="Destí 2019" width="370" height="900" />

        <h2>Destí</h2>
        
        <p><strong>Motivació: </strong>Saber llegir quan la vida et porta a situacions que mai haguessis imaginat, i saber treure d’aquestes el millor possible perquè el destí així ho ha decidit en el teu camí de vida.</p>
        
        <p><strong>Fitxes tècniques</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../uploads/2021/A4-desti-2021.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Destí 2021</h3>
            </li>
            <li>
                <a class="pdf-file" href="../uploads/2022/A4-desti-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Destí 2022</h3>
            </li>
        </ul>
        
    </div>
</div>


<div class="gridder-content" id="llibertat">
    <div class="gridder-copy">
        <img src="../assets/img/vins-2022/llibertat-2022-large.jpg" class="media-v" alt="Fades del granit 2020" width="370" height="900" />

        <h2>Llibertat</h2>
        
        <p><strong>Motivació:</strong> Donar culte a la Carinyena negra com una de les millors varietats autòctones de l'Empordà, la terra que va donar la Llibertat creativa a Còsmic el 2013. Còsmic sempre li retrà homenatge fent aquest vi des del cor i l'exercici de la llibertat.</p>
        
        <p><strong>Fitxes tècniques</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../uploads/2022/A4-llibertat-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Llibertat 2022</h3>
            </li>
        </ul>
        
    </div>
</div>


<div class="gridder-content" id="connexio-cosmica">
    <div class="gridder-copy">
        <img src="../assets/img/vins-2019/connexio-cosmica-2019-large.jpg" class="media-v" alt="Connexió Còsmica 2019" width="370" height="900" />

        <h2>Connexió Còsmica</h2>
        
        <p><strong>Motivació:</strong> Es un vi que neix fruit d’unes circumstàncies. La poca valoració de l’agricultura de qualitat en general i els baixos preus del raïm, van fer néixer aquest vi, amb la intenció de dignificar el treball de la pagesia i tancar cercles entre el pagès i el consumidor final. És un vi que neix del treball en equip i de la més profunda estima per la Terra, amb l’objectiu de dignificar-la.</p>
        
        <p><strong>Fitxes tècniques</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../uploads/connexio-cosmica-2019-ca.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Connexió Còsmica 2019</h3>
            </li>
            <li>
                <a class="pdf-file" href="../uploads/2020/a4-connexio-cosmica-2020.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Connexió Còsmica 2020</h3>
            </li>
        </ul>

    </div>
</div>

<div class="gridder-content" id="essencia">
    <div class="gridder-copy">
        <img src="../assets/img/vi-essencia-large.jpg" class="media-v" alt="Essència 2019" width="370" height="900" />

        <h2>Essència</h2>
        
        <p><strong>Motivació:</strong> Retre culte a la carinyena blanca en forma de vi dolç, buscant la seva essència més profunda en absència de d´aigua, endinsar-se en uns horitzons on els sucres i l’acidesa són extrems, i juguen en un equilibri de desequilibris on el punt mig ens transporta a l’essencial.</p>
        
        <p><strong>Fitxes tècniques</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../uploads/essencia-2019-ca.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Essència 2019</h3>
            </li>
        </ul>
        
    </div>
</div>

<div class="gridder-content" id="paciencia-ancestral">
    <div class="gridder-copy">
        <img src="../assets/img/vi-paciencia-large.jpg" class="media-v" alt="Paciència Ancestral 2017" width="370" height="900" />

        <h2>Paciència Ancestral</h2>
        
        <p><strong>Motivació:</strong> El valor del saber esperar al moment adequat, en aquest vi volem retre culte a la carinyena blanca en format espumós, acompanyar la puresa que hi ha darrera les vinificacions ancestrals i estar atents al diàleg existent entre el temps i el vi, per saber trobar el moment adequat i poder brindar amb el fruits de la recompensa d’haver sigut pacients.</p>
        
        <p><strong>Fitxes tècniques</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../uploads/paciencia-ancestral-2017-ca.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Paciència Ancestral 2017</h3>
            </li>
        </ul>
        
    </div>
</div>

<div class="gridder-content" id="paisatge-cosmic">
    <div class="gridder-copy">
        <img src="../assets/img/vins-2020/paisatge-cosmic-2020-large.jpg" class="media-v" alt="Paisatge Còsmic" width="370" height="900" />

        <h2>Paisatge Còsmic</h2>
        <p><strong>Motivació:</strong> Fer un vi que capti el perfum del l’entorn que envolta les nostres vinyes amb una mirada renovada, que et faci viatjar al paisatge de la nostra vinya mitjançant aquest vi.</p>
        
        <p><strong>Fitxes tècniques</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../uploads/2020/a4-paisatge-cosmic-2018.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Paisatge Còsmic</h3>
            </li>
        </ul>
    </div>
</div>

<div class="gridder-content" id="vitalitat">
    <div class="gridder-copy">
        <img src="../assets/img/vins-2019/vitalitat-2019-large.jpg" class="media-v" alt="Vitalitat 2019" width="370" height="900" />

        <h2>Vitalitat</h2>
        
        <p><strong>Motivació:</strong> Fer un vi <em style="font-style:italic;">petillant</em>, revitalitzant, elèctric, fresc, festiu i “xispejant”, donant valor a les varietats autòctones catalanes amb una mirada renovada.</p>
        
        <p><strong>Fitxes tècniques</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../uploads/2021/A4-vitalitat-2021.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Vitalitat 2021</h3>
            </li>
            <li>
                <a class="pdf-file" href="../uploads/2022/A4-vitalitat-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Vitalitat 2022</h3>
            </li>
        </ul>
        
    </div>
</div>

<div class="gridder-content" id="fades-del-granit">
    <div class="gridder-copy">
        <img src="../assets/img/vins-2020/fades-del-granit-2020-large.jpg" class="media-v" alt="Fades del granit 2020" width="370" height="900" />

        <h2>Fades del granit</h2>
        
        <p><strong>Motivació:</strong> Fer un vi <em style="font-style:italic;">petillant</em>, revitalitzant, elèctric, fresc, festiu i “xispejant”, donant valor a les varietats autòctones catalanes amb una mirada renovada.</p>
        
        <p><strong>Fitxes tècniques</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../uploads/2021/A4-fades-del-granit-2021.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Fades del granit 2021</h3>
            </li>
            <li>
                <a class="pdf-file" href="../uploads/2022/A4-fades-del-granit-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Fades del granit 2022</h3>
            </li>
        </ul>
        
    </div>
</div>




<div class="gridder-content" id="fades-del-calcari">
    <div class="gridder-copy">
        <img src="../assets/img/vins-2022/fades-del-calcari-2022-large.jpg" class="media-v" alt="Fades del granit 2020" width="370" height="900" />

        <h2>Fades del Calcari</h2>
        
        <p><strong>Motivació:</strong> Fer un vi <em style="font-style:italic;">petillant</em> revitalitzant, elèctric, profund, mineral i xispejant que parli dels terrenys calcaris, donant valor a les varietats autòctones del Penedès amb una mirada renovada.</p>
        
        <p><strong>Fitxes tècniques</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../uploads/2022/A4-fades-del-calcari-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Fades del Calcari 2022</h3>
            </li>
        </ul>
        
    </div>
</div>