<div class="gridder-content" id="consciencia-cosmica">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2020/consciencia-cosmica-2020-large.jpg" class="media-v" alt="Consciència Còsmica" width="370" height="900" />

        <h2>Consciència Còsmica</h2>
        <p><strong>Motivació:</strong> Rendir culto a la Cariñena gris en estado puro. Iniciar la creación de un nuevo terroir de altura, en el cielo del Empordà, a 600 metros de altura en una zona virgen y llena de biodiversidad.</p>
        
        <p><strong>Fichas técnicas</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/2021/A4-consciencia-cosmica-2021.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Consciència Còsmica 2021</h3>
            </li>
            <li>
                <a class="pdf-file" href="../../uploads/2022/A4-consciencia-cosmica-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Consciència Còsmica 2022</h3>
            </li>
        </ul>
    </div>
</div>

<div class="gridder-content" id="constelacio">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2020/constelacio-2020-large.jpg" class="media-v" alt="Constel·lació" width="370" height="900" />

        <h2>Constel·lació</h2>
        <p><strong>Motivació:</strong> Rendir homenaje a todas la variedades autóctonas ampurdanesas recuperadas que cultivamos en una misma parcela de nueva plantación, construyendo un vino con todas ellas juntas, encontrando la belleza del equilibrio que existe en la diversidad.</p>
        
        <p><strong>Fichas técnicas</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/2021/A4-constelacio-2021.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Constel·lació 2021</h3>
            </li>
            <li>
                <a class="pdf-file" href="../../uploads/2022/A4-constelacio-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Constel·lació 2022</h3>
            </li>
        </ul>
    </div>
</div>

<div class="gridder-content" id="el-teu-mar">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2020/el-teu-mar-2020-large.jpg" class="media-v" alt="El teu mar les meves muntanyes" width="370" height="900" />

        <h2>El teu mar les meves muntanyes</h2>
        <p><strong>Motivació:</strong> Es un vino que nace de la amistad. Es un abrazo entre las garnachas negras, blancas y grises de la Yoyo (el mar) de Banyuls, y las cariñenas blancas, grises y negras de Còsmic (las montañas) de Agullana, materializando así nuestra amistad uniendo mar y montaña en un vino.</p>
        
        <p><strong>Fichas técnicas</strong></p>

        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/2021/A4-el-teu-mar-les-meves-muntanyes-2021.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>El teu mar les meves muntanyes 2021</h3>
            </li>
            <li>
                <a class="pdf-file" href="../../uploads/2022/A4-el-teu-mar-les-meves-muntanyes-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>El teu mar les meves muntanyes 2022</h3>
            </li>
        </ul>
    </div>
</div>

<div class="gridder-content" id="gratitud-sauvignon-blanc">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2017/gratitud-sauvignon-blanc-2016-large.jpg" class="media-v" alt="Gratitud Sauvignon blanc 2019" width="370" height="900" />

        <h2>Gratitud blanc</h2>
        
        <p><strong>Motivación:</strong> Gratitud a la tierra y las personas con las que he nacido y he crecido, apertura al Mundo integrando las variedades internacionales que mejor se adaptan a nuestra tierra y climatología.</p>

        <p><strong>Fichas técnicas</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/2021/A4-gratitud-blanc-2021 .pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Gratitud blanc 2021</h3>
            </li>
            <li>
                <a class="pdf-file" href="../../uploads/2022/A4-gratitud-blanc-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Gratitud blanc 2022</h3>
            </li>
        </ul>
        
    </div>
</div>

<div class="gridder-content" id="gratitud-cabernet-franc">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2017/gratitud-cabernet-franc-2016-large.jpg" class="media-v" alt="Gratitud Cabernet franc 2019" width="370" height="900" />

        <h2>Gratitud negre</h2>
        
        <p><strong>Motivación:</strong> Gratitud a la tierra y las personas con las que he nacido y he crecido, apertura al Mundo integrando las variedades internacionales que mejor se adaptan a nuestra tierra y climatología.</p>
        
        <p><strong>Fichas técnicas</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/gratitud-cabernet-franc-2019-es.pdf" title="Ficha del vino en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Gratitud Cabernet franc 2019</h3>
            </li>
            <li>
                <a class="pdf-file" href="../../uploads/2020/a4-gratitud-cabernet-franc-2020.pdf" title="Ficha del vino en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Gratitud Cabernet franc 2020</h3>
            </li>
        </ul>
        
    </div>
</div>

<div class="gridder-content" id="confianca-garnatxa-roja">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2019/confiansa-2019-large.jpg" class="media-v" alt="Confiança Garnatxa roja 2019" width="370" height="900" />

        <h2>Confiança</h2>
        
        <p><strong>Motivación: </strong>rendir culto a la Garnacha Roja, el primer paso en el camino estacional de hacer vino, la primera interpretación de todo un año de trabajo en la tierra abrazando el valor de la confianza, y saltar al vacío de lo desconocido año tras año para seguir volando en su cielo, donde el color es el rosa del sol que se pone para volver a nacer.</p>
        
        <p><strong>Fichas técnicas</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/2021/A4-confiansa-2021.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Confiança 2021</h3>
            </li>
            <li>
                <a class="pdf-file" href="../../uploads/2022/A4-confiansa-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Confiança 2022</h3>
            </li>
        </ul>

    </div>
</div>

<div class="gridder-content" id="valentia-carinyena-blanca">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2019/valentia-2019-large.jpg" class="media-v" alt="Valentia Carinyena blanca 2019" width="370" height="900" />

        <h2>Valentia</h2>

        <p><strong>Motivación:</strong> Rendir culto a la cariñena blanca, sobreponer el instinto y la intuición con la mayor fuerza y valor ante los condicionamientos establecidos que nos rodean y que nos limitan, para poder ir adentro, en profundidad, hacia lo que estamos destinados a ser, nuestra identidad real, la conexión con nuestra tierra y nuestro ser, sin miedos.</p>
        
        
        <p><strong>Fichas técnicas</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/2021/A4-valentia-2021.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Valentia 2021</h3>
            </li>
            <li>
                <a class="pdf-file" href="../../uploads/2022/A4-valentia-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Valentia 2022</h3>
            </li>
        </ul>

    </div>
</div>

<div class="gridder-content" id="via-fora-macabeu">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2019/via-fora-macabeu-2019-large.jpg" class="media-v" alt="Via Fora Macabeu 2019" width="370" height="900" />

        <h2>Via Fora blanc</h2>
        
        <p><strong>Motivación:</strong> Es un vino que nace de la firmeza en las situaciones difíciles de la vida, donde nuestra identidad puede quedar reducida al olvido. Es un homenaje a los agricultores sabios, que han sido firmes conservadores de nuestro patrimonio cultural y vitivinícola en todo el territorio de nuestro querido país.</p>
        
        <p><strong>Fichas técnicas</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/2021/A4-via-fora-blanc-2021.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Via Fora blanc 2021</h3>
            </li>
            <li>
                <a class="pdf-file" href="../../uploads/2022/A4-via-fora-blanc-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Via Fora blanc 2022</h3>
            </li>
        </ul>
        
    </div>
</div>

<div class="gridder-content" id="via-fora-sumoll">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2019/via-fora-sumoll-2019-large.jpg" class="media-v" alt="Via Fora Sumoll 2019" width="370" height="900" />

        <h2>Via Fora Negre</h2>
        
        <p><strong>Motivación:</strong> Es un vino que nace de la firmeza en las situaciones difíciles de la vida, donde nuestra identidad puede quedar reducida al olvido. Es un homenaje a los agricultores sabios, que han sido firmes conservadores de nuestro patrimonio cultural y vitivinícola en todo el territorio de nuestro querido país.</p>
        
        <p><strong>Fichas técnicas</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/via-fora-sumoll-2019-es.pdf" title="Ficha del vino en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Via Fora Sumoll 2019</h3>
            </li>
            <li>
                <a class="pdf-file" href="../../uploads/2020/a4-via-fora-sumoll-2020.pdf" title="Ficha del vino en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Via Fora Sumoll 2020</h3>
            </li>
        </ul>
        
    </div>
</div>

<div class="gridder-content" id="passio-marcelan">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2019/passio-2019-large.jpg" class="media-v" alt="Passió 2019" width="370" height="900" />

        <h2>Passió</h2>
        
        <p><strong>Motivación:</strong> La Passió es el vino que nace fruto del sentimiento de amor a la viña y a las tierras que trabajo y que dominan mi voluntad y razón, es el nexo de unión entre todos los vinos que hago, el valor que hace de motor entre todos ellos y también es un abrazo de complicidad entre los dos territorios que interpreto con pasión absoluta, mis orígenes y mi presente.</p>
        
        <p><strong>Fichas técnicas</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/2021/A4-passio-2021.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Passió 2021</h3>
            </li>
            <li>
                <a class="pdf-file" href="../../uploads/2022/A4-passio-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Passió 2022</h3>
            </li>
        </ul>
        
    </div>
</div>

<div class="gridder-content" id="encarinyades">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2019/encarinyades-2019-large.jpg" class="media-v" alt="Encarinyades 2019" width="370" height="900" />

        <h2>Encarinyades</h2>
        
        <p><strong>Motivación: </strong>Recuperar de nuevo las genéticas de la cariñena blanca, negra y gris, jugando con ellas, reinterpretar su magia, disfrutar de su juego de complicidad, intentar hacer un vino donde se muestre la delicadeza y la profundidad de unas variedades autóctonas que son una gran parte del ADN del Empordà, donde sueño despierto.</p>
        
        <p><strong>Fichas técnicas</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/2021/A4-encarinyades-2021.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Encarinyades 2021</h3>
            </li>
            <li>
                <a class="pdf-file" href="../../uploads/2022/A4-encarinyades-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Encarinyades 2022</h3>
            </li>
        </ul>
        
    </div>
</div>

<div class="gridder-content" id="desti">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2019/desti-2019-large.jpg" class="media-v" alt="Destí 2019" width="370" height="900" />

        <h2>Destí</h2>
        
        <p><strong>Motivación: </strong>Saber leer cuando la vida te lleva a situaciones que nunca hubieras imaginado, y saber sacar de estas lo mejor posible porqué el destino así lo ha decidido en tu camino de vida.</p>
        
        <p><strong>Fichas técnicas</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/2021/A4-desti-2021.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Destí 2021</h3>
            </li>
            <li>
                <a class="pdf-file" href="../../uploads/2022/A4-desti-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Destí 2022</h3>
            </li>
        </ul>
        
    </div>
</div>


<div class="gridder-content" id="llibertat">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2022/llibertat-2022-large.jpg" class="media-v" alt="Fades del granit 2020" width="370" height="900" />

        <h2>Llibertat</h2>
        
        <p><strong>Motivación:</strong> Rendir culto a la Cariñena tinta como una de las mejores variedades autóctonas del Empordà, la tierra que dio la libertad creativa a Còsmic en el 2013. Còsmic siempre le rendirá homenaje haciendo este vino desde el corazón y el ejercicio de la libertad.</p>
        
        <p><strong>Fichas técnicas</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/2022/A4-llibertat-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Llibertat 2022</h3>
            </li>
        </ul>
        
    </div>
</div>



<div class="gridder-content" id="connexio-cosmica">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2019/connexio-cosmica-2019-large.jpg" class="media-v" alt="Connexió Còsmica 2019" width="370" height="900" />

        <h2>Connexió Còsmica</h2>
        
        <p><strong>Motivación:</strong> Es un vino que nace fruto de unas circunstancias. La poca valoración de la agricultura de calidad en general y los bajos precios de la uva, hicieron nacer este vino, con la intención de dignificar el trabajo de los campesinos y cerrar círculos entre el agricultor y el consumidor final. Es un vino que nace del trabajo en equipo y de la más profunda estima por la Tierra, con el objetivo de dignificarla.</p>
        
        <p><strong>Fichas técnicas</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/connexio-cosmica-2019-es.pdf" title="Ficha del vino en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Connexió Còsmica 2019</h3>
            </li>
            <li>
                <a class="pdf-file" href="../../uploads/2020/a4-connexio-cosmica-2020.pdf" title="Ficha del vino en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Connexió Còsmica 2020</h3>
            </li>
        </ul>
        
    </div>
</div>

<div class="gridder-content" id="essencia">
    <div class="gridder-copy">
        <img src="../../assets/img/vi-essencia-large.jpg" class="media-v" alt="Essència 2019" width="370" height="900" />

        <h2>Essència</h2>
        
        <p><strong>Motivación:</strong> Rendir culto a la cariñena blanca en forma de vino dulce, buscando su esencia más profunda en ausencia de agua, adentrarse en unos horizontes donde los azúcares y la acidez son extremos, y juegan en un equilibrio de desequilibrios donde el punto medio nos transporta a lo esencial.</p>
        
        <p><strong>Fichas técnicas</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/essencia-2019-es.pdf" title="Ficha del vino en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Essència 2019</h3>
            </li>
        </ul>
        
    </div>
</div>

<div class="gridder-content" id="paciencia-ancestral">
    <div class="gridder-copy">
        <img src="../../assets/img/vi-paciencia-large.jpg" class="media-v" alt="Paciència Ancestral 2017" width="370" height="900" />

        <h2>Paciència Ancestral</h2>
        
        <p><strong>Motivación:</strong> El valor del saber esperar el momento adecuado, en este vino queremos rendir culto a la cariñena blanca en formato espumoso, acompañar la pureza que hay detrás de las vinificaciones ancestrales y estar atentos al diálogo existente entre el tiempo y el vino, para saber encontrar el momento adecuado y poder brindar con el frutos de la recompensa de haber sido pacientes.</p>
        
        <p><strong>Fichas técnicas</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/paciencia-ancestral-2017-es.pdf" title="Ficha del vino en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Paciència Ancestral 2017</h3>
            </li>
        </ul>
        
    </div>
</div>

<div class="gridder-content" id="paisatge-cosmic">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2020/paisatge-cosmic-2020-large.jpg" class="media-v" alt="Paisatge Còsmic" width="370" height="900" />

        <h2>Paisatge Còsmic</h2>
        <p><strong>Motivación:</strong> Hacer un vino que capte el perfume del entorno que rodea nuestras viñas con una mirada renovada, que te haga viajar al paisaje de nuestra viña a través de este vino.</p>
        
        <p><strong>Fichas técnicas</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/2020/a4-paisatge-cosmic-2018.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Consciència Còsmica</h3>
            </li>
        </ul>
    </div>
</div>

<div class="gridder-content" id="vitalitat">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2019/vitalitat-2019-large.jpg" class="media-v" alt="Vitalitat 2019" width="370" height="900" />

        <h2>Vitalitat</h2>
        
        <p><strong>Motivación:</strong> Hacer un vino <em>petillant</em>, revitalizante, eléctrico, fresco, festivo y chispeante, dando valor a las variedades autóctonas catalanas con una mirada renovada.</p>
        
        <p><strong>Fichas técnicas</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/2021/A4-vitalitat-2021.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Vitalitat 2021</h3>
            </li>
            <li>
                <a class="pdf-file" href="../../uploads/2022/A4-vitalitat-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Vitalitat 2022</h3>
            </li>
        </ul>
        
    </div>
</div>

<div class="gridder-content" id="fades-del-granit">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2020/fades-del-granit-2020-large.jpg" class="media-v" alt="Fades del granit" width="370" height="900" />

        <h2>Fades del granit</h2>
        
        <p><strong>Motivación:</strong> Hacer un vino <em>petillant</em>, revitalizante, eléctrico, fresco, festivo y chispeante, dando valor a las variedades autóctonas catalanas con una mirada renovada.</p>
        
        <p><strong>Fichas técnicas</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/2021/A4-fades-del-granit-2021.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Fades del granit 2021</h3>
            </li>
            <li>
                <a class="pdf-file" href="../../uploads/2022/A4-fades-del-granit-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Fades del granit 2022</h3>
            </li>
        </ul>
        
    </div>
</div>





<div class="gridder-content" id="fades-del-calcari">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2022/fades-del-calcari-2022-large.jpg" class="media-v" alt="Fades del granit 2020" width="370" height="900" />

        <h2>Fades del Calcari</h2>
        
        <p><strong>Motivación:</strong> Hacer un vino <em style="font-style:italic;">petillant</em> revitalizante, eléctrico, profundo, mineral y chispeante, que hable de los terrenos calcáreos, dando valor a las variedades autóctonas del Penedés con una mirada renovada.</p>
        
        <p><strong>Fichas técnicas</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/2022/A4-fades-del-calcari-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Fades del Calcari 2022</h3>
            </li>
        </ul>
        
    </div>
</div>