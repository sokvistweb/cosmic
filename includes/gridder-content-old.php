<div class="gridder-content" id="gratitud-sauvignon-blanc">
    <div class="gridder-copy">
        <img src="../assets/img/vins-2017/gratitud-sauvignon-blanc-2016-large.jpg" class="media-v" alt="Gratitud Sauvignon blanc 2019" width="370" height="900" />

        <h2>Gratitud Sauvignon blanc 2019</h2>
        <p><strong>Motivació:</strong> Gratitud a la terra i les persones amb les que he nascut i he crescut, obertura al Món integrant les varietats internacionals que millor s’adapten a la nostra terra i climatologia.</p>
        <p><strong>Edició limitada:</strong> 5.200 ampolles</p>
        <p><strong>Raïm:</strong> 100% sauvignon blanc (parcel·la eco certificada)</p>
        <p><strong>Vinya:</strong> El Coll d’Olivera (Serra del Montmell), Baix Penedès</p>
        <p><strong>Alçada:</strong> 550 m</p>
        <p><strong>Terreny:</strong> argila vermella calcària molt compacta</p>
        <p><strong>Vinificació:</strong> raïm collit a mà amb caixes de 10 kg. Maceració amb pells durant 2 dies, suau escorregut del most sense premsar.</p>
        <p><strong>Fermentació:</strong> espontània 70%, seleccionat 30%</p>
        <p><strong>Recipient fermentació:</strong> 10% àmfora, 20% castanyer i 70% inoxidable</p>
        <p><strong>Sulfits total:</strong> 28 mg/l</p>
        <p><strong>Alcohol:</strong> 12,5%</p>
        <p><strong>Vi no filtrat</strong></p>
        <a class="pdf-file" href="../uploads/gratitud-sauvignon-blanc-2019-ca.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>

<div class="gridder-content" id="gratitud-cabernet-franc">
    <div class="gridder-copy">
        <img src="../assets/img/vins-2017/gratitud-cabernet-franc-2016-large.jpg" class="media-v" alt="Gratitud Cabernet franc 2019" width="370" height="900" />

        <h2>Gratitud Cabernet franc 2019</h2>
        
        <p><strong>Motivació:</strong> Gratitud a la terra i les persones amb les que he nascut i he crescut, obertura al Món integrant  les varietats internacionals que millor s’adapten a la nostra terra i climatologia.</p>
        <p><strong>Edició limitada:</strong> 4.690 ampolles</p>
        <p><strong>Raïm:</strong> 85% cabernet franc, 15% parellada (parcel·la eco certificada)</p>
        <p><strong>Vinya:</strong> Parcel·la La Solana (Serra del Montmell), Baix Penedès</p>
        <p><strong>Alçada:</strong> 500 m</p>
        <p><strong>Terreny:</strong> argila vermella calcària molt compacta.</p>
        <p><strong>Vinificació:</strong> verema manual amb caixes de 15 kg. Maceració amb pell i raspó durant 7 dies, posterior sangrat i fermentació 50% en àmfora i 50% en inoxidable</p>
        <p><strong>Fermentació:</strong> espontània (llevat autòcton)</p>
        <p><strong>Criança:</strong> afinament en bota de castanyer durant 2,5 mesos</p>
        <p><strong>Recipient fermentació:</strong> 70% àmfora, 30% inoxidable</p>
        <p><strong>No filtrat, sulfits total:</strong> 8 mg/l</p>
        <p><strong>Grau alcohòlic:</strong> 12,5%</p>
        
        <a class="pdf-file" href="../uploads/gratitud-cabernet-franc-2019-ca.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>

<div class="gridder-content" id="confianca-garnatxa-roja">
    <div class="gridder-copy">
        <img src="../assets/img/vins-2019/confiansa-2019-large.jpg" class="media-v" alt="Confiança Garnatxa roja 2019" width="370" height="900" />

        <h2>Confiança 2019</h2>
        
        <p><strong>Motivació: </strong>retre culte a la Garnatxa Roja, el primer pas en el camí estacional de fer vi, la primera interpretació de tot un any de treball a la terra abraçant el valor de la confiança, i saltar al buit del desconegut any rere any per a seguir volant en el seu cel, on el color és el rosa del sol que es pon per a tornar a néixer.</p>
        <p><strong>Edició limitada:</strong> 1.995 ampolles</p>
        <p><strong>Raïm:</strong> 100% Garnatxa Roja</p>
        <p><strong>Agricultura:</strong> ecològica amb criteris biodinàmics</p>
        <p><strong>Vinya:</strong> en vas de 50 anys</p>
        <p><strong>Paratges:</strong> Prat d’Egna, Agullana (Alt Empordà)</p>
        <p><strong>Alçada:</strong>  290 m.</p>
        <p><strong>Terreny:</strong> granític arenós</p>
        <p><strong>Vinificació:</strong>  verema manual amb caixes de 15 kg -maceració amb pells durant 3 dies, premsat molt suau, fermentació 40% àmfora, 60% inoxidable.</p>
        <p><strong>Fermentació:</strong> espontània (llevat autòcton)</p>
        <p><strong>Criança:</strong> dipòsit inoxidable sobre lies fines</p>
        <p><strong>Recipient fermentació:</strong> inoxidable, àmfora</p>
        <p><strong>No filtrat, sulfits total:</strong> 8mg/l.</p>
        <p><strong>Grau alcohòlic:</strong> 12%</p>

        <a class="pdf-file" href="../uploads/confiansa-2019-ca.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>

<div class="gridder-content" id="valentia-carinyena-blanca">
    <div class="gridder-copy">
        <img src="../assets/img/vins-2019/valentia-2019-large.jpg" class="media-v" alt="Valentia Carinyena blanca 2019" width="370" height="900" />

        <h2>Valentia 2019</h2>

        <p><strong>Motivació:</strong> Retre culte a la carinyena blanca, sobreposar l’instint i la intuïció amb la major força i valor davant els condicionaments establerts que ens envolten i que ens limiten, per poder anar endins, en profunditat, cap allò que estem destinats a ser, la nostra identitat real, la connexió amb la nostra terra i el nostre ser, sense pors.</p>
        <p><strong>Edició limitada: </strong>2.590 ampolles</p>
        <p><strong>Raïm: </strong>100% carinyena blanca</p>
        <p><strong>Agricultura: </strong>ecològica amb criteris biodinàmics</p>
        <p><strong>Vinya: </strong>en vas de 60 anys</p>
        <p><strong>Paratges: </strong>Prat d’Egna - l’Estrada, Agullana (Alt Empordà)</p>
        <p><strong>Alçada: </strong>290m</p>
        <p><strong>Terreny: </strong>granític arenós</p>
        <p><strong>Vinificació</strong>: verema manual amb caixes de 15 kg. Maceració amb pells durant 4 dies, premsat suau, fermentació espontània 60% inoxidable, 20% àmfora i 20% castanyer.</p>
        <p><strong>Fermentació: </strong>espontània (llevat autòcton)</p>
        <p><strong>Criança: </strong>50% inoxidable sobre lies fines, 50% bota de castanyer 3 mesos</p>
        <p><strong>Recipient fermentació: </strong>inoxidable, àmfora, bota de castanyer</p>
        <p><strong>No filtrat, sulfits total: </strong>8mg/l</p>
        <p><strong>Grau alcohòlic: </strong>12%</p>

        <a class="pdf-file" href="../uploads/valentia-2019-ca.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>

<div class="gridder-content" id="via-fora-macabeu">
    <div class="gridder-copy">
        <img src="../assets/img/vins-2019/via-fora-macabeu-2019-large.jpg" class="media-v" alt="Via Fora Macabeu 2019" width="370" height="900" />

        <h2>Via Fora Macabeu 2019</h2>
        
        <p><strong>Motivació:</strong> Es un vi que neix de la fermesa en les situacions difícils de la vida, on la nostra identitat pot quedar reduïda a l’oblit. És un homenatge als pagesos savis, que han sigut ferms conservadors del nostre patrimoni cultural i vitivinícola arreu del territori del nostre estimat país.</p>
        <p><strong>Edició limitada: </strong>2.000 ampolles</p>
        <p><strong>Raïm: </strong>100% macabeu</p>
        <p><strong>Agricultura: </strong>ecològica amb certificació</p>
        <p><strong>Vinya: </strong>en vas de 75 anys</p>
        <p><strong>Paratge: </strong>El Pauet - Rodonyà - Serra del Montmell ( Alt Camp)</p>
        <p><strong>Terreny: </strong>costers d’argiles blanques calcàries</p>
        <p><strong>Vinificació: </strong>collit a mà amb caixes de 15 kg. Maceració amb pells i part del la rapa durant 5 dies, escorregut suau sense premsat, fermentació espontània en dipòsit d’inoxidable.</p>
        <p><strong>Fermentació: </strong>llevat autòcton</p>
        <p><strong>Recipient fermentació: </strong>dipòsit inoxidable</p>
        <p><strong>Criança: </strong>sobre lies fines en dipòsit inoxidable</p>
        <p><strong>No filtrat, no clarificat, sulfits total: </strong>&lt;10 mg/l</p>
        <p><strong>Grau alcohòlic: </strong>11,5%</p>
        
        <a class="pdf-file" href="../uploads/via-fora-macabeu-2019-ca.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>

<div class="gridder-content" id="via-fora-sumoll">
    <div class="gridder-copy">
        <img src="../assets/img/vins-2019/via-fora-sumoll-2019-large.jpg" class="media-v" alt="Via Fora Sumoll 2019" width="370" height="900" />

        <h2>Via Fora sumoll 2019</h2>
        
        <p><strong>Motivació:</strong> Es un vi que neix de la fermesa en les situacions difícils de la vida, on la nostra identitat pot quedar reduïda a l’oblit. És un homenatge als pagesos savis, que han sigut ferms conservadors del nostre patrimoni cultural i vitivinícola arreu del territori del nostre estimat país.</p>
        <p><strong>Edició limitada: </strong>1.290 ampolles</p>
        <p><strong>Raïm: </strong>100% sumoll</p>
        <p><strong>Agricultura: </strong>ecològica amb criteris biodinàmics</p>
        <p><strong>Vinya:</strong> vinya en vas de 56 anys</p>
        <p><strong>Paratge: </strong>El Cup d’en Benet, Serra del Montmell (Baix Penedès)</p>
        <p><strong>Terreny: </strong>argilo calcari de poca profunditat</p>
        <p><strong>Vinificació: </strong>collit a mà amb caixes de 15 kg. Maceració de raïm sencer durant 7 dies, (maceració carbònica) premsat suau, fermentació espontània en dipòsit d’inoxidable 70% i àmfora 30%.</p>
        <p><strong>Fermentació: </strong>llevat autòcton</p>
        <p><strong>Recipient fermentació: </strong>dipòsit inoxidable, àmfora</p>
        <p><strong>Criança: </strong>en botes de castanyer durant 4 mesos</p>
        <p><strong>No filtrat, no clarificat, sulfits total: </strong>&lt;10 mg/l</p>
        <p><strong>Grau alcohòlic: </strong>12%</p>
        
        <a class="pdf-file" href="../uploads/via-fora-sumoll-2019-ca.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>

<div class="gridder-content" id="passio-marcelan">
    <div class="gridder-copy">
        <img src="../assets/img/vins-2019/passio-2019-large.jpg" class="media-v" alt="Passió 2019" width="370" height="900" />

        <h2>Passió 2019</h2>
        
        <p><strong>Motivació:</strong> La Passió és el vi que neix fruit del sentiment d’amor a la vinya i a les terres que treballo i que dominen la meva voluntat i raó, és el nexe d’unió entre tots els vins que faig, el valor que fa de motor entre tots ells i també és una abraçada de complicitat entre els dos territoris que interpreto amb passió absoluta, el meus orígens i el meu present.</p>
        <p><strong>Edició limitada: </strong>2.702</p>
        <p><strong>Raïm: </strong>80% marselan, 20% carinyena negra</p>
        <p><strong>Agricultura: </strong>ecològica amb criteris biodinàmics</p>
        <p><strong>Vinya: </strong>20 anys formació emparrada, 6 anys formació en vas</p>
        <p><strong>Paratges: </strong>Serra del Montmell (Baix Penedès), Prat d’Egna Agullana (Alt Empordà)</p>
        <p><strong>Alçada: </strong>500m, 280m</p>
        <p><strong>Terreny: </strong>argilo calcari, granític arenós</p>
        <p><strong>Vinificació: </strong>verema manual amb caixes de 15 kg. Maceració raïm sencer sense derrapar ni trepitjar (maceració carbònica) durant 9 dies, premsat i fermentació espontània en 30% àmfores i 70% dipòsit d’inoxidable.</p>
        <p><strong>Fermentació: </strong>espontània (llevat autòcton)</p>
        <p><strong>Criança: </strong>50% bota de castanyer, 50% inoxidable sobre lies fines</p>
        <p><strong>Recipient fermentació: </strong>inoxidable, àmfora</p>
        <p><strong>No filtrat, sulfits total: </strong>8mg/l</p>
        <p><strong>Grau alcohòlic: </strong>12%</p>
        
        <a class="pdf-file" href="../uploads/passio-2019-ca.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>

<div class="gridder-content" id="llibertat">
    <div class="gridder-copy">
        <img src="../assets/img/vins-2019/encarinyades-2019-large.jpg" class="media-v" alt="Encarinyades 2019" width="370" height="900" />

        <h2>Encarinyades 2019</h2>
        
        <p><strong>Motivació: </strong>Recuperar de nou les genètiques de la carinyena blanca, negra i gris, tot jugant amb elles, reinterpretar la seva màgia, gaudir del seu joc de complicitat, intentar de fer un vi on es mostri la delicadesa i la profunditat d’unes varietats autòctones que són una gran part de l’ADN de l’Empordà, on somio despert.</p>
        <p><strong>Edició limitada:</strong> 3.750 ampolles</p>
        <p><strong>Raïm:</strong> 80% carinyena blanca, 15% carinyena negra i 5% carinyena gris (parcel·la eco certificada)</p>
        <p><strong>Vinya:</strong> en vas 6 anys, 60 anys, 20 anys</p>
        <p><strong>Paratge: </strong>Prat d’Egna, Agullana (Alt Empordà)</p>
        <p><strong>Alçada: </strong>300m</p>
        <p><strong>Terreny: </strong>granític arenós</p>
        <p><strong>Vinificació: </strong>verema manual amb caixes de 15kg. Maceració amb pell i raspó durant 5 dies, premsat i fermentació. 100% en àmfora de fang.</p>
        <p><strong>Fermentació: </strong>espontània (llevat autòcton)</p>
        <p><strong>Criança:</strong> 70% dipòsit d’inoxidable, 30% bota de castanyer durant 2,5 mesos</p>
        <p><strong>Recipient fermentació: </strong>100% àmfora</p>
        <p><strong>No filtrat, sulfits total: </strong>8mg/l</p>
        <p><strong>Grau alcohòlic: </strong>12 %</p>
        
        <a class="pdf-file" href="../uploads/encarinyades-2019-ca.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>

<div class="gridder-content" id="desti">
    <div class="gridder-copy">
        <img src="../assets/img/vins-2019/desti-2019-large.jpg" class="media-v" alt="Destí 2019" width="370" height="900" />

        <h2>Destí 2019</h2>
        
        <p><strong>Motivació: </strong>Saber llegir quan la vida et porta a situacions que mai haguessis imaginat, i saber treure d’aquestes el millor possible perquè el destí així ho ha decidit en el teu camí de vida.</p>
        <p><strong>Edició limitada: </strong>5.350 ampolles</p>
        <p><strong>Raïm: </strong>100% Moscatell d’Alexandria (parcel·la eco certificada)</p>
        <p><strong>Vinya: </strong>La vinya del Joan, Capmany (Alt Empordà)</p>
        <p><strong>Alçada: </strong>122m.</p>
        <p><strong>Terreny: </strong>granític arenós</p>
        <p><strong>Vinificació:</strong> verema manual amb caixes de 15kg. Maceració amb raïm sencer durant 3 setmanes.</p>
        <p><strong>Fermentació: </strong>espontània (llevat autòcton)</p>
        <p><strong>Criança: </strong>sobre lies fines en dipòsit d’inoxidable</p>
        <p><strong>Recipient fermentació: </strong>inoxidable</p>
        <p><strong>No filtrat, sulfits total: </strong>8mg/l.</p>
        <p><strong>Grau alcohòlic: </strong>11,5%</p>
        
        <a class="pdf-file" href="../uploads/desti-2019-ca.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>

<div class="gridder-content" id="connexio-cosmica">
    <div class="gridder-copy">
        <img src="../assets/img/vins-2019/connexio-cosmica-2019-large.jpg" class="media-v" alt="Connexió Còsmica 2019" width="370" height="900" />

        <h2>Connexió Còsmica 2019</h2>
        
        <p><strong>Motivació:</strong> Es un vi que neix fruit d’unes circumstàncies. La poca valoració de l’agricultura de qualitat en general i els baixos preus del raïm, van fer néixer aquest vi, amb la intenció de dignificar el treball de la pagesia i tancar cercles entre el pagès i el consumidor final. És un vi que neix del treball en equip i de la més profunda estima per la Terra, amb l’objectiu de dignificar-la.</p>
    <p><strong>Edició limitada:</strong> 9.400 ampolles</p>
    <p><strong>Raïm: </strong>85% Xarel·lo, 10% Chardonnay i 5% Moscatell d’Alexandria (parcel·la eco certificada)</p>
    <p><strong>Paratges: </strong>Camp del Boter (Rodonyà), Serra del Montmell, les Parellades (Penedès)</p>
    <p><strong>Alçades: </strong>300 i 500m.</p>
    <p><strong>Terreny:</strong> argilo calcari</p>
    <p><strong>Vinificació:</strong> verema manual. Maceració amb pell i part de raspó durant 8 dies, premsat i fermentat en dipòsit d’inoxidable.</p>
    <p><strong>Fermentació: </strong>espontània amb llevat autòcton</p>
    <p><strong>Criança:</strong> sobre lies fines en dipòsit inoxidable</p>
    <p><strong>No filtrat, sulfits total:</strong> 10mg/l.</p>
    <p><strong>Grau alcohòlic:</strong> 12%</p>
        
        <a class="pdf-file" href="../uploads/connexio-cosmica-2019-ca.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>

<div class="gridder-content" id="essencia">
    <div class="gridder-copy">
        <img src="../assets/img/vi-essencia-large.jpg" class="media-v" alt="Essència 2019" width="370" height="900" />

        <h2>Essència 2019</h2>
        
        <p><strong>Motivació:</strong> Retre culte a la carinyena blanca en forma de vi dolç, buscant la seva essència més profunda en absència de d´aigua, endinsar-se en uns horitzons on els sucres i l’acidesa són extrems, i juguen en un equilibri de desequilibris on el punt mig ens transporta a l’essencial.</p>
        <p><strong>Edició limitada:</strong> 1.390 ampolles</p>
        <p><strong>Raïm:</strong> 100% carinyena blanca</p>
        <p><strong>Agricultura:</strong> ecològica amb criteris biodinàmics</p>
        <p><strong>Vinya:</strong> en vas de 60 anys</p>
        <p><strong>Paratges:</strong> Prat d’Egna - L’ Estrada, Agullana (Alt Empordà)</p>
        <p><strong>Alçada:</strong> 290 m</p>
        <p><strong>Terreny:</strong> granític arenós</p>
        <p><strong>Vinificació:</strong> verema primerenca, buscant un baix contingut en sucres i un alt contingut en acidesa. Maceració amb pells durant 4 dies, premsat, i reducció del most per ebullició molt lenta, on reduirem un 70% el volum original concentrant sucres i acidesa. Posterior refredament i ressembra amb most flor original per iniciar una suau i lenta fermentació que durarà molts mesos fins al seu final.</p>
        <p><strong>Fermentació:</strong> espontània (llevat autòcton)</p>
        <p><strong>Criança:</strong> oxidativa en dipòsit inoxidable obert i damajoana de vidre</p>
        <p><strong>Recipient de fermentació:</strong> inoxidable, damajoana de vidre</p>
        <p><strong>No filtrat, no conté sulfits afegits </strong></p>
        <p><strong>Sucres residuals:</strong> 410 g/litre</p>
        <p><strong>Acidesa total:</strong> 9,5 ATS</p>
        <p><strong>Grau alcohòlic:</strong> 9%</p>
        
        <a class="pdf-file" href="../uploads/essencia-2019-ca.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>

<div class="gridder-content" id="paciencia-ancestral">
    <div class="gridder-copy">
        <img src="../assets/img/vi-paciencia-large.jpg" class="media-v" alt="Paciència Ancestral 2017" width="370" height="900" />

        <h2>Paciència Ancestral 2017</h2>
        
        <p><strong>Motivació:</strong> El valor del saber esperar al moment adequat, en aquest vi volem retre culte a la carinyena blanca en format espumós, acompanyar la puresa que hi ha darrera les vinificacions ancestrals i estar atents al diàleg existent entre el temps i el vi, per saber trobar el moment adequat i poder brindar amb el fruits de la recompensa d’haver sigut pacients.</p>
        <p><strong>Edició Limitada:</strong> 2.000 ampolles</p>
        <p><strong>Raïm:</strong> 100% carinyena blanca</p>
        <p><strong>Agricultura:</strong> ecològica amb criteris biodinàmics</p>
        <p><strong>Vinya:</strong> en vas de 60 anys.</p>
        <p><strong>Paratges:</strong> Prat d’Egna, Agullana (Alt Empordà)</p>
        <p><strong>Alçada:</strong> 290 m</p>
        <p><strong>Terreny:</strong> granític arenós</p>
        <p><strong>Vinificació:</strong> verema manual amb caixes de 15 kg. Maceració amb pells durant 2 dies, escorregut suau sense premsar, fermentació  espontània en àmfores de fang amb final de fermentació i criança en ampolla, aclarit en pupitre manual, desgorjat manual i taponat amb suro amb posterior repòs fins el seu consum.</p>
        <p><strong>Fermentació:</strong> espontània (llevat autòcton)</p>
        <p><strong>Criança:</strong> 24 mesos en ampolla</p>
        <p><strong>Recipient fermentació:</strong> àmfora, ampolla de vidre</p>
        <p><strong>No filtrat, no conté sulfits afegits</strong></p>
        <p><strong>Grau Alcohòlic:</strong> 12%</p>
        
        <a class="pdf-file" href="../uploads/paciencia-ancestral-2017-ca.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>

<div class="gridder-content" id="vitalitat">
    <div class="gridder-copy">
        <img src="../assets/img/vins-2019/vitalitat-2019-large.jpg" class="media-v" alt="Vitalitat 2019" width="370" height="900" />

        <h2>Vitalitat 2019</h2>
        
        <p><strong>Motivació:</strong> Fer un vi <em>petillant</em>, revitalitzant, elèctric, fresc, festiu i “xispejant”, donant valor a les varietats autòctones catalanes amb una mirada renovada.</p>
        <p><strong>Edició limitada: </strong>3.100 ampolles</p>
        <p><strong>Raïm: </strong>92% parellada, 8% moscatell (parcel·la eco certificada)</p>
        <p><strong>Vinya: </strong>50 anys i 15 anys</p>
        <p><strong>Paratge: </strong>Serra del Montmell (Baix Penedès)</p>
        <p><strong>Alçada: </strong>500m</p>
        <p><strong>Terreny: </strong>argilo calcari</p>
        <p><strong>Vinificació: </strong>verema manual amb caixes de 15 kg. Derrapat i maceració amb pells durant 3 dies, escorregut suau del most sense premsar.</p>
        <p><strong>Fermentació: </strong>espontània (llevat autòcton) amb final de fermentació a l’ampolla</p>
        <p><strong>Criança: </strong>sobre lies fines a l’ampolla fins el moment del seu consum</p>
        <p><strong>Recipient fermentació: </strong>inoxidable i ampolla de vidre</p>
        <p><strong>No filtrat, sulfits total: </strong>8mg/l</p>
        <p><strong>Grau alcohòlic: </strong>11%</p>
        
        <a class="pdf-file" href="../uploads/vitalitat-2019-ca.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>