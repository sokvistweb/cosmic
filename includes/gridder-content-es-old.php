<div class="gridder-content" id="gratitud-sauvignon-blanc">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2017/gratitud-sauvignon-blanc-2016-large.jpg" class="media-v" alt="Gratitud Sauvignon blanc 2019" width="370" height="900" />

        <h2>Gratitud Sauvignon blanc 2019</h2>
        
        <p><strong>Motivación:</strong> Gratitud a la tierra y las personas con las que he nacido y he crecido, apertura al Mundo integrando las variedades internacionales que mejor se adaptan a nuestra tierra y climatología.</p>

        <p><strong>Edición limitada: </strong>5.200 botellas</p>
        <p><strong>Uva: </strong>100% sauvignon blanc (parcela eco certificada)</p>
        <p><strong>Viña: </strong>El Coll d’Olivera (Serra del Montmell), Baix Penedès</p>
        <p><strong>Altura: </strong>550 m</p>
        <p><strong>Terreno: </strong>arcilla roja caliza muy compacta</p>
        <p><strong>Vinificación: </strong>uvas cosechadas a mano con cajas de 10 kg. Maceración con pieles durante 2 días, suave escurrido del mosto sin prensar.</p>
        <p><strong>Fermentación: </strong>espontánea 70%, seleccionado 30%</p>
        <p><strong>Recipiente fermentación: </strong>10% ánfora, 20% castaño y 70% inoxidable</p>
        <p><strong>Sulfitos total: </strong>28 mg/l</p>
        <p><strong>Alcohol: </strong>12,5%</p>
        <p><strong>Vino no filtrado</strong></p>
        
        <a class="pdf-file" href="../../uploads/gratitud-sauvignon-blanc-2019-es.pdf" title="Ficha del vino en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>

<div class="gridder-content" id="gratitud-cabernet-franc">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2017/gratitud-cabernet-franc-2016-large.jpg" class="media-v" alt="Gratitud Cabernet franc 2019" width="370" height="900" />

        <h2>Gratitud Cabernet franc 2019</h2>
        
        <p><strong>Motivación:</strong> Gratitud a la tierra y las personas con las que he nacido y he crecido, apertura al Mundo integrando las variedades internacionales que mejor se adaptan a nuestra tierra y climatología.</p>
        <p><strong>Edición limitada: </strong>4.690 botellas</p>
        <p><strong>Uva: </strong>85% cabernet franc, 15% parellada (parcela eco certificada)</p>
        <p><strong>Viña: </strong>Parcel·la La Solana (Serra del Montmell), Baix Penedès</p>
        <p><strong>Altura: </strong>500 m</p>
        <p><strong>Terreno: </strong>arcilla roja caliza muy compacta</p>
        <p><strong>Vinificación: </strong>vendimia manual en cajas de 15 kg. Maceración con piel y raspas durante 7 días, posterior sangrado y fermentación 50% en ánfora y 50% en inoxidable</p>
        <p><strong>Fermentación: </strong>espontánea (levadura autóctona)</p>
        <p><strong>Crianza: </strong>afinamiento en bota de castaño durante 2,5 meses</p>
        <p><strong>Recipiente fermentación: </strong>70% ánfora, 30% inoxidable</p>
        <p><strong>No filtrado, sulfitos total: </strong>8 mg/l</p>
        <p><strong>Grado alcohólico: </strong>12,5%</p>
        
        <a class="pdf-file" href="../../uploads/gratitud-cabernet-franc-2019-es.pdf" title="Ficha del vino en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>

<div class="gridder-content" id="confianca-garnatxa-roja">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2019/confiansa-2019-large.jpg" class="media-v" alt="Confiança Garnatxa roja 2019" width="370" height="900" />

        <h2>Confiança 2019</h2>
        
        <p><strong>Motivación: </strong>rendir culto a la Garnacha Roja, el primer paso en el camino estacional de hacer vino, la primera interpretación de todo un año de trabajo en la tierra abrazando el valor de la confianza, y saltar al vacío de lo desconocido año tras año para seguir volando en su cielo, donde el color es el rosa del sol que se pone para volver a nacer.</p>
        <p><strong>Edición limitada: </strong>1.995 botellas</p>
        <p><strong>Uva:</strong> 100% Garnacha Roja</p>
        <p><strong>Agricultura: </strong>ecológica con criterios biodinámicos</p>
        <p><strong>Viña:</strong> en vaso de 50 años</p>
        <p><strong>Parajes: </strong>Prat d’Egna, Agullana (Alt Empordà)</p>
        <p><strong>Altura:</strong> 290 m.</p>
        <p><strong>Terreno: </strong>granítico arenoso</p>
        <p><strong>Vinificación: </strong>vendimia manual en cajas de 15 kg -maceración con pieles durante 3 días, prensado muy suave, fermentación 40% ánfora, 60% inoxidable.</p>
        <p><strong>Fermentación: </strong>espontánea (levadura autóctona)</p>
        <p><strong>Crianza: </strong>depósito inoxidable sobre lías finas</p>
        <p><strong>Recipiente fermentación:</strong> inoxidable, ánfora</p>
        <p><strong>No filtrado, sulfitos total:</strong> 8 mg / l.</p>
        <p><strong>Grado alcohólico:</strong> 12%</p>

        <a class="pdf-file" href="../../uploads/confiansa-2019-es.pdf" title="Ficha del vino en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>

<div class="gridder-content" id="valentia-carinyena-blanca">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2019/valentia-2019-large.jpg" class="media-v" alt="Valentia Carinyena blanca 2019" width="370" height="900" />

        <h2>Valentia 2019</h2>

        <p><strong>Motivación:</strong> Rendir culto a la cariñena blanca, sobreponer el instinto y la intuición con la mayor fuerza y valor ante los condicionamientos establecidos que nos rodean y que nos limitan, para poder ir adentro, en profundidad, hacia lo que estamos destinados a ser, nuestra identidad real, la conexión con nuestra tierra y nuestro ser, sin miedos.</p>
        <p><strong>Edición limitada: </strong>2.590 botellas</p>
        <p><strong>Uva: </strong>100% cariñena blanca</p>
        <p><strong>Agricultura: </strong>ecológica con criterios biodinámicos</p>
        <p><strong>Viña: </strong>en vaso de 60 años</p>
        <p><strong>Parajes: </strong>Prat d’Egna – l’Estrada, Agullana (Alt Empordà)</p>
        <p><strong>Altura: </strong>290m</p>
        <p><strong>Terreno: </strong>granítico arenoso</p>
        <p><strong>Vinificación: </strong>vendimia manual en cajas de 15 kg. Maceración con pieles durante 4 días, prensado suave, fermentación espontánea 60% inoxidable, 20% ánfora y 20% castaño.</p>
        <p><strong>Fermentación: </strong>espontánea (levadura autóctona)</p>
        <p><strong>Crianza: </strong>50% inoxidable sobre lías finas, 50% bota de castaño 3 meses</p>
        <p><strong>Recipiente fermentación: </strong>inoxidable, ánfora, bota de castaño</p>
        <p><strong>No filtrado, sulfitos total: </strong>8 mg/l</p>
        <p><strong>Grado alcohólico: </strong>12%</p>

        <a class="pdf-file" href="../../uploads/valentia-2019-es.pdf" title="Ficha del vino en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>

<div class="gridder-content" id="via-fora-macabeu">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2019/via-fora-macabeu-2019-large.jpg" class="media-v" alt="Via Fora Macabeu 2019" width="370" height="900" />

        <h2>Via Fora Macabeu 2019</h2>
        
        <p><strong>Motivación:</strong> Es un vino que nace de la firmeza en las situaciones difíciles de la vida, donde nuestra identidad puede quedar reducida al olvido. Es un homenaje a los agricultores sabios, que han sido firmes conservadores de nuestro patrimonio cultural y vitivinícola en todo el territorio de nuestro querido país.</p>
        <p><strong>Edición limitada: </strong>2.000 botellas</p>
        <p><strong>Uva: </strong>100% macabeo</p>
        <p><strong>Agricultura: </strong>ecológica con certificación</p>
        <p><strong>Viña: </strong>en vaso de 75 años</p>
        <p><strong>Paraje: </strong>El Pauet - Rodonyà - Sierra de Montmell (Alt Camp)</p>
        <p><strong>Terreno: </strong>laderas de arcillas blancas calizas</p>
        <p><strong>Vinificación: </strong>cosechado a mano con cajas de 15 kg. Maceración con pieles y parte del raspón durante 5 días, escurrido suave sin prensado, fermentación espontánea en depósito de inoxidable.</p>
        <p><strong>Fermentación: </strong>levadura autóctona</p>
        <p><strong>Recipiente fermentación: </strong>depósito inoxidable</p>
        <p><strong>Crianza: </strong>sobre lías finas en depósito inoxidable</p>
        <p><strong>No filtrado, no clarificado, sulfitos total: </strong>&lt;10 mg/l</p>
        <p><strong>Grado alcohólico: </strong>11,5%</p>
        
        <a class="pdf-file" href="../../uploads/via-fora-macabeu-2019-es.pdf" title="Ficha del vino en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>

<div class="gridder-content" id="via-fora-sumoll">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2019/via-fora-sumoll-2019-large.jpg" class="media-v" alt="Via Fora Sumoll 2019" width="370" height="900" />

        <h2>Via Fora Sumoll 2019</h2>
        
        <p><strong>Motivación:</strong> Es un vino que nace de la firmeza en las situaciones difíciles de la vida, donde nuestra identidad puede quedar reducida al olvido. Es un homenaje a los agricultores sabios, que han sido firmes conservadores de nuestro patrimonio cultural y vitivinícola en todo el territorio de nuestro querido país.</p>
        <p><strong>Edición limitada: </strong>1.290 botellas</p>
        <p><strong>Uva: </strong>100% sumoll</p>
        <p><strong>Agricultura: </strong>ecológica con criterios biodinámicos</p>
        <p><strong>Viña: </strong>viña en vaso de 56 años</p>
        <p><strong>Paraje: </strong>El Cup d’en Benet, Serra del Montmell (Baix Penedès)</p>
        <p><strong>Terreno: </strong>arcillo calcáreo de poca profundidad</p>
        <p><strong>Vinificación: </strong>cosechado a mano con cajas de 15 kg. Maceración de uva entera durante 7 días, (maceración carbónica) prensado suave, fermentación espontánea en depósito de inoxidable 70% y ánfora 30%.</p>
        <p><strong>Fermentación: </strong>levadura autóctona</p>
        <p><strong>Recipiente fermentación: </strong>depósito inoxidable, ánfora</p>
        <p><strong>Crianza: </strong>en barricas de castaño durante 4 meses</p>
        <p><strong>No filtrado, no clarificado, sulfitos total: </strong>&lt;10 mg/l</p>
        <p><strong>Grado alcohólico: </strong>12%</p>
        
        <a class="pdf-file" href="../../uploads/via-fora-sumoll-2019-es.pdf" title="Ficha del vino en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>

<div class="gridder-content" id="passio-marcelan">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2019/passio-2019-large.jpg" class="media-v" alt="Passió 2019" width="370" height="900" />

        <h2>Passió 2019</h2>
        
        <p><strong>Motivación:</strong> La Passió es el vino que nace fruto del sentimiento de amor a la viña y a las tierras que trabajo y que dominan mi voluntad y razón, es el nexo de unión entre todos los vinos que hago, el valor que hace de motor entre todos ellos y también es un abrazo de complicidad entre los dos territorios que interpreto con pasión absoluta, mis orígenes y mi presente.</p>
        <p><strong>Edición limitada: </strong>2.702</p>
        <p><strong>Uva: </strong>80% marselan, 20% cariñena negra</p>
        <p><strong>Agricultura: </strong>ecológica con criterios biodinámicos</p>
        <p><strong>Viña: </strong>20 años formación emparrada, 6 años formación en vaso</p>
        <p><strong>Parajes: </strong>Serra del Montmell (Baix Penedès), Prat d’Egna Agullana (Alt Empordà)</p>
        <p><strong>Altura: </strong>500m, 280m</p>
        <p><strong>Terreno: </strong>arcillo calcáreo, granítico arenoso</p>
        <p><strong>Vinificación: </strong>vendimia manual en cajas de 15 kg. Maceración uva entera sin derrapar ni pisar (maceración carbónica) durante 9 días, prensado y fermentación espontánea en 30% ánforas y 70% depósito de inoxidable.</p>
        <p><strong>Fermentación: </strong>espontánea (levadura autóctona)</p>
        <p><strong>Crianza: </strong>50% bota de castaño, 50% inoxidable sobre lías finas</p>
        <p><strong>Recipiente fermentación: </strong>inoxidable, ánfora</p>
        <p><strong>No Filtrado, Sulfitos total: </strong>8mg/l</p>
        <p><strong>Grado Alcohólico: </strong>12%</p>
        
        <a class="pdf-file" href="../../uploads/passio-2019-es.pdf" title="Ficha del vino en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>

<div class="gridder-content" id="llibertat">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2019/encarinyades-2019-large.jpg" class="media-v" alt="Encarinyades 2019" width="370" height="900" />

        <h2>Encarinyades 2019</h2>
        
        <p><strong>Motivación: </strong>Recuperar de nuevo las genéticas de la cariñena blanca, negra y gris, jugando con ellas, reinterpretar su magia, disfrutar de su juego de complicidad, intentar hacer un vino donde se muestre la delicadeza y la profundidad de unas variedades autóctonas que son una gran parte del ADN del Empordà, donde sueño despierto.</p>
        <p><strong>Edición limitada: </strong>3.750 botellas</p>
        <p><strong>Uva: </strong>80% cariñena blanca, 15% cariñena negra y 5% cariñena gris (parcela eco certificada)</p>
        <p><strong>Viña: </strong>en vaso 6 años, 60 años, 20 años</p>
        <p><strong>Paraje: </strong>Prat d’Egna, Agullana (Alt Empordà)</p>
        <p><strong>Altura: </strong>300m</p>
        <p><strong>Terreno: </strong>granítico arenoso</p>
        <p><strong>Vinificación: </strong>vendimia manual en cajas de 15kg. Maceración con piel y raspas durante 5 días, prensado y fermentación. 100% en ánfora de barro.</p>
        <p><strong>Fermentación: </strong>espontánea (levadura autóctona)</p>
        <p><strong>Crianza:</strong> 70% depósito de inoxidable, 30% bota de castaño durante 2,5 meses</p>
        <p><strong>Recipiente fermentación:</strong> 100% ánfora</p>
        <p><strong>No filtrado, sulfitos total:</strong> 8 mg/l</p>
        <p><strong>Grado alcohólico:</strong> 12%</p>
        
        <a class="pdf-file" href="../../uploads/encarinyades-2019-es.pdf" title="Ficha del vino en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>

<div class="gridder-content" id="desti">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2019/desti-2019-large.jpg" class="media-v" alt="Destí 2019" width="370" height="900" />

        <h2>Destí 2019</h2>
        
        <p><strong>Motivación: </strong>Saber leer cuando la vida te lleva a situaciones que nunca hubieras imaginado, y saber sacar de estas lo mejor posible porqué el destino así lo ha decidido en tu camino de vida.</p>
        <p><strong>Edición limitada:</strong> 5.350 botellas</p>
        <p><strong>Uva:</strong> 100% Moscatel de Alejandría (parcela eco certificada)</p>
        <p><strong>Viña: </strong>La vinya del Joan, Capmany (Alt Empordà)</p>
        <p><strong>Altura:</strong> 122m.</p>
        <p><strong>Terreno: </strong>granítico arenoso</p>
        <p><strong>Vinificación: </strong>vendimia manual en cajas de 15kg. Maceración con uva entera durante 3 semanas.</p>
        <p><strong>Fermentación:</strong> espontánea (levadura autóctona)</p>
        <p><strong>Crianza:</strong> sobre lías finas en depósito de inoxidable</p>
        <p><strong>Recipiente fermentación: </strong>inoxidable</p>
        <p><strong>No filtrado, sulfitos total:</strong> 8mg/l.</p>
        <p><strong>Grado alcohólico:</strong> 11,5%</p>
        
        <a class="pdf-file" href="../../uploads/desti-2019-es.pdf" title="Ficha del vino en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>

<div class="gridder-content" id="connexio-cosmica">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2019/connexio-cosmica-2019-large.jpg" class="media-v" alt="Connexió Còsmica 2019" width="370" height="900" />

        <h2>Connexió Còsmica 2019</h2>
        
        <p><strong>Motivación:</strong> Es un vino que nace fruto de unas circunstancias. La poca valoración de la agricultura de calidad en general y los bajos precios de la uva, hicieron nacer este vino, con la intención de dignificar el trabajo de los campesinos y cerrar círculos entre el agricultor y el consumidor final. Es un vino que nace del trabajo en equipo y de la más profunda estima por la Tierra, con el objetivo de dignificarla.</p>
        <p><strong>Edición limitada:</strong> 9.400 botellas</p>
        <p><strong>Uva: </strong>85% Xarel·lo, 10% Chardonnay y 5% Moscatel de Alejandría (parcela eco certificada)</p>
        <p><strong>Parajes: </strong>Camp del Boter (Rodonyà), Serra del Montmell, les Parellades (Penedès)</p>
        <p><strong>Alturas: </strong>300 y 500m.</p>
        <p><strong>Terreno: </strong>arcillo calcáreo</p>
        <p><strong>Vinificación: </strong>vendimia manual. Maceración con piel y parte de raspado durante 8 días, prensado y fermentado en depósito de inoxidable.</p>
        <p><strong>Fermentación: </strong>espontánea con levadura autóctona</p>
        <p><strong>Crianza: </strong>sobre lías finas en depósito inoxidable</p>
        <p><strong>No filtrado, sulfitos total: </strong>10mg/l.</p>
        <p><strong>Grado alcohólico:</strong> 12%</p>
        
        <a class="pdf-file" href="../../uploads/connexio-cosmica-2019-es.pdf" title="Ficha del vino en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>

<div class="gridder-content" id="essencia">
    <div class="gridder-copy">
        <img src="../../assets/img/vi-essencia-large.jpg" class="media-v" alt="Essència 2019" width="370" height="900" />

        <h2>Essència 2019</h2>
        
        <p><strong>Motivación:</strong> Rendir culto a la cariñena blanca en forma de vino dulce, buscando su esencia más profunda en ausencia de agua, adentrarse en unos horizontes donde los azúcares y la acidez son extremos, y juegan en un equilibrio de desequilibrios donde el punto medio nos transporta a lo esencial.</p>
        <p><strong>Edición limitada: </strong>1.390 botellas</p>
        <p><strong>Uva: </strong>100% cariñena blanca</p>
        <p><strong>Agricultura: </strong>ecológica con criterios biodinámicos</p>
        <p><strong>Viña: </strong>en vaso de 60 años</p>
        <p><strong>Parajes: </strong>Prat d’Egna - L’ Estrada, Agullana (Alt Empordà)</p>
        <p><strong>Altura: </strong>290 m</p>
        <p><strong>Terreno: </strong>granítico arenoso</p>
        <p><strong>Vinificación: </strong>vendimia temprana, buscando un bajo contenido en azúcares y un alto contenido en acidez. Maceración con pieles durante 4 días, prensado, y reducción del mosto por ebullición muy lenta, donde reduciremos un 70% el volumen original concentrando azúcares y acidez. Posterior enfriamiento y resiembra con mosto flor original para iniciar una suave y lenta fermentación que durará muchos meses hasta su final.</p>
        <p><strong>Fermentación: </strong>espontánea (levadura autóctona)</p>
        <p><strong>Crianza: </strong>oxidativa en depósito inoxidable abierto y damajuanas de vidrio</p>
        <p><strong>Recipiente de fermentación: </strong>inoxidable, damajuanas de vidrio</p>
        <p><strong>No filtrado, no contiene sulfitos añadidos</strong></p>
        <p><strong>Azúcares residuales: </strong>410 g/litro</p>
        <p><strong>Acidez total: </strong>9,5 ATS</p>
        <p><strong>Grado alcohólico: </strong>9%</p>
        
        <a class="pdf-file" href="../../uploads/essencia-2019-es.pdf" title="Ficha del vino en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>

<div class="gridder-content" id="paciencia-ancestral">
    <div class="gridder-copy">
        <img src="../../assets/img/vi-paciencia-large.jpg" class="media-v" alt="Paciència Ancestral 2017" width="370" height="900" />

        <h2>Paciència Ancestral 2017</h2>
        
        <p><strong>Motivación:</strong> El valor del saber esperar el momento adecuado, en este vino queremos rendir culto a la cariñena blanca en formato espumoso, acompañar la pureza que hay detrás de las vinificaciones ancestrales y estar atentos al diálogo existente entre el tiempo y el vino, para saber encontrar el momento adecuado y poder brindar con el frutos de la recompensa de haber sido pacientes.</p>
        <p><strong>Edición Limitada: </strong>2.000 botellas</p>
        <p><strong>Uva: </strong>100% cariñena blanca</p>
        <p><strong>Agricultura: </strong>ecológica con criterios biodinámicos</p>
        <p><strong>Viña: </strong>en vaso de 60 años</p>
        <p><strong>Parajes: </strong>Prat d’Egna, Agullana (Alt Empordà)</p>
        <p><strong>Altura: </strong>290 m</p>
        <p><strong>Terreno: </strong>granítico arenoso</p>
        <p><strong>Vinificación: </strong>vendimia manual en cajas de 15 kg. Maceración con pieles durante 2 días, escurrido suave sin prensar, fermentación espontánea en ánforas de barro con final de fermentación y crianza en botella, aclarado en pupitre manual, degüelle manual y taponado con corcho con posterior reposo hasta su consumo.</p>
        <p><strong>Fermentación: </strong>espontánea (levadura autóctona)</p>
        <p><strong>Crianza: </strong>24 meses en botella</p>
        <p><strong>Recipiente fermentación: </strong>ánfora, botella de cristal</p>
        <p><strong>No filtrado, no contiene sulfitos añadidos</strong></p>
        <p><strong>Grado Alcohólico: </strong>12%</p>
        
        <a class="pdf-file" href="../../uploads/paciencia-ancestral-2017-es.pdf" title="Ficha del vino en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>

<div class="gridder-content" id="vitalitat">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2019/vitalitat-2019-large.jpg" class="media-v" alt="Vitalitat 2019" width="370" height="900" />

        <h2>Vitalitat 2019</h2>
        
        <p><strong>Motivación:</strong> Hacer un vino <em>petillant</em>, revitalizante, eléctrico, fresco, festivo y chispeante, dando valor a las variedades autóctonas catalanas con una mirada renovada.</p>
        <p><strong>Edición limitada: </strong>3.100 botellas</p>
        <p><strong>Uva: </strong>92% parellada, 8% moscatel (parcela eco certificada)</p>
        <p><strong>Viña: </strong>50 años y 15 años</p>
        <p><strong>Paraje: </strong>Sierra de Montmell (Baix Penedès)</p>
        <p><strong>Altura: </strong>500m</p>
        <p><strong>Terreno: </strong>arcillo calcáreo</p>
        <p><strong>Vinificación: </strong>vendimia manual en cajas de 15 kg. Derrapado y maceración con pieles durante 3 días, escurrido suave del mosto sin prensar.</p>
        <p><strong>Fermentación: </strong>espontánea (levadura autóctona) con final de fermentación en la botella</p>
        <p><strong>Crianza: </strong>sobre lías finas a la botella hasta el momento de su consumo</p>
        <p><strong>Recipiente fermentación: </strong>inoxidable y botella de vidrio</p>
        <p><strong>No filtrado, sulfitos total: </strong>8 mg/l</p>
        <p><strong>Grado alcohólico: </strong>11%</p>
        
        <a class="pdf-file" href="../../uploads/vitalitat-2019-es.pdf" title="Ficha del vino en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>