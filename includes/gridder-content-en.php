<div class="gridder-content" id="consciencia-cosmica">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2020/consciencia-cosmica-2020-large.jpg" class="media-v" alt="Consciència Còsmica" width="370" height="900" />

        <h2>Consciència Còsmica</h2>
        <p><strong>Motivació:</strong> To worship the Carignan gris in its purest state. Start the creation of a new high altitude terroir, in the Empordà sky, 600 meters above sea level in a virgin area full of biodiversity.</p>
        
        <p><strong>Tech sheets</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/2021/A4-consciencia-cosmica-2021.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Consciència Còsmica 2021</h3>
            </li>
            <li>
                <a class="pdf-file" href="../../uploads/2022/A4-consciencia-cosmica-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Consciència Còsmica 2022</h3>
            </li>
        </ul>
    </div>
</div>

<div class="gridder-content" id="constelacio">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2020/constelacio-2020-large.jpg" class="media-v" alt="Constel·lació" width="370" height="900" />

        <h2>Constel·lació</h2>
        <p><strong>Motivació:</strong> To pay tribute to all the native Empordà varieties that we have recovered and we grow alltogether in a newly planted plot, building a wine with all of them together, finding the beauty of the balance that exists in diversity.</p>
        
        <p><strong>Tech sheets</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/2021/A4-constelacio-2021.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Constel·lació 2021</h3>
            </li>
            <li>
                <a class="pdf-file" href="../../uploads/2022/A4-constelacio-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Constel·lació 2022</h3>
            </li>
        </ul>
    </div>
</div>

<div class="gridder-content" id="el-teu-mar">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2020/el-teu-mar-2020-large.jpg" class="media-v" alt="El teu mar les meves muntanyes" width="370" height="900" />

        <h2>El teu mar les meves muntanyes</h2>
        <p><strong>Motivació:</strong> This is a wine that is born from friendship. It is an embrace between the black, white and gray Grenache of Yoyo (the sea) of Banyuls, and the white, gray and black Carignans of Còsmic (the mountains) of Agullana, thus materializing our friendship and uniting sea and mountain in a wine.</p>
        
        <p><strong>Tech sheets</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/2021/A4-el-teu-mar-les-meves-muntanyes-2021.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>El teu mar les meves muntanyes 2021</h3>
            </li>
            <li>
                <a class="pdf-file" href="../../uploads/2022/A4-el-teu-mar-les-meves-muntanyes-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>El teu mar les meves muntanyes 2022</h3>
            </li>
        </ul>
    </div>
</div>

<div class="gridder-content" id="gratitud-sauvignon-blanc">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2017/gratitud-sauvignon-blanc-2016-large.jpg" class="media-v" alt="Gratitud Sauvignon blanc 2019" width="370" height="900" />

        <h2>Gratitud blanc</h2>
        
        <p><strong>Motivation: </strong>Gratitude to the land and the people with whom I was born and raised, opening to the world integrating the international varieties that best adapt to our land and climate.</p>
        
        <p><strong>Tech sheets</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/2021/A4-gratitud-blanc-2021 .pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Gratitud blanc 2021</h3>
            </li>
            <li>
                <a class="pdf-file" href="../../uploads/2022/A4-gratitud-blanc-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Gratitud blanc 2022</h3>
            </li>
        </ul>
        
    </div>
</div>

<div class="gridder-content" id="gratitud-cabernet-franc">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2017/gratitud-cabernet-franc-2016-large.jpg" class="media-v" alt="Gratitud Cabernet franc 2019" width="370" height="900" />

        <h2>Gratitud negre</h2>
        
        <p><strong>Motivation: </strong>Gratitude to the land and the people with whom I was born and raised, opening to the world integrating the international varieties that best adapt to our land and climate.</p>
        
        <p><strong>Tech sheets</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/gratitud-cabernet-franc-2019-en.pdf" title="Wine sheet in PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Gratitud Sauvignon franc 2019</h3>
            </li>
            <li>
                <a class="pdf-file" href="../../uploads/2020/a4-gratitud-cabernet-franc-2020.pdf" title="Wine sheet in PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Gratitud Sauvignon franc 2020</h3>
            </li>
        </ul>
        
    </div>
</div>

<div class="gridder-content" id="confianca-garnatxa-roja">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2019/confiansa-2019-large.jpg" class="media-v" alt="Confiança Garnatxa roja 2016" width="370" height="900" />

        <h2>Confiança</h2>
        
        <p><strong>Motivation: </strong>worshiping the Red Grenache, the first step on the seasonal path of making wine, the first interpretation of a whole year of work on earth embracing the value of trust, and jumping into the void of the unknown year after year to keep flying in its sky, where the color is the pink of the sun that sets to be reborn.</p>
        
        <p><strong>Tech sheets</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/2021/A4-confiansa-2021.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Confiança 2021</h3>
            </li>
            <li>
                <a class="pdf-file" href="../../uploads/2022/A4-confiansa-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Confiança 2022</h3>
            </li>
        </ul>

    </div>
</div>

<div class="gridder-content" id="valentia-carinyena-blanca">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2019/valentia-2019-large.jpg" class="media-v" alt="Valentia Carinyena blanca 2019" width="370" height="900" />

        <h2>Valentia Carinyena blanca</h2>

        <p><strong>Motivation:</strong> Worship the white cariñena, overlap instinct and intuition with the greatest force and value before the established conditions that surround us and that limit us, to be able to go deep into what we are destined to be, our real identity, the connection with our land and our being, without fear.</p>
        
        <p><strong>Tech sheets</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/2021/A4-valentia-2021.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Valentia 2021</h3>
            </li>
            <li>
                <a class="pdf-file" href="../../uploads/2022/A4-valentia-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Valentia 2022</h3>
            </li>
        </ul>

    </div>
</div>

<div class="gridder-content" id="via-fora-macabeu">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2019/via-fora-macabeu-2019-large.jpg" class="media-v" alt="Via Fora Macabeu 2019" width="370" height="900" />

        <h2>Via Fora blanc</h2>
        
        <p><strong>Motivation:</strong> It is a wine that is born from firmness in the difficult situations of life, where our identity can be reduced to oblivion. It is a tribute to wise farmers, who have been firm conservators of our cultural and wine heritage throughout the territory of our beloved country.</p>
        
        <p><strong>Tech sheets</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/2021/A4-via-fora-blanc-2021.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Via Fora blanc 2021</h3>
            </li>
            <li>
                <a class="pdf-file" href="../../uploads/2022/A4-via-fora-blanc-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Via Fora blanc 2022</h3>
            </li>
        </ul>
        
    </div>
</div>

<div class="gridder-content" id="via-fora-sumoll">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2019/via-fora-sumoll-2019-large.jpg" class="media-v" alt="Via Fora Sumoll 2019" width="370" height="900" />

        <h2>Via Fora negre</h2>
        
        <p><strong>Motivation:</strong> It is a wine that is born from firmness in the difficult situations of life, where our identity can be reduced to oblivion. It is a tribute to wise farmers, who have been firm conservators of our cultural and wine heritage throughout the territory of our beloved country.</p>
        
        <p><strong>Tech sheets</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/via-fora-sumoll-2019-en.pdf" title="Wine sheet in PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Via Fora Sumoll 2019</h3>
            </li>
            <li>
                <a class="pdf-file" href="../../uploads/2020/a4-via-fora-sumoll-2020.pdf" title="Wine sheet in PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Via Fora Sumoll 2020</h3>
            </li>
        </ul>
        
    </div>
</div>

<div class="gridder-content" id="passio-marcelan">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2019/passio-2019-large.jpg" class="media-v" alt="Passió 2019" width="370" height="900" />

        <h2>Passió</h2>
        
        <p><strong>Motivation:</strong> Passió is the wine that was born as a result of the feeling of love for the vineyard and the lands that I work and that dominate my will and my reason, it is the link between all the wines I make, the motor value among all them and it is also a hug of complicity between the two territories that I interpret with absolute passion, my origins and my present.</p>
        
        <p><strong>Tech sheets</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/2021/A4-passio-2021.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Passió 2021</h3>
            </li>
            <li>
                <a class="pdf-file" href="../../uploads/2022/A4-passio-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Passió 2022</h3>
            </li>
        </ul>
        
    </div>
</div>

<div class="gridder-content" id="encarinyades">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2019/encarinyades-2019-large.jpg" class="media-v" alt="Encarinyades 2019" width="370" height="900" />

        <h2>Encarinyades</h2>
        
        <p><strong>Motivation: </strong>Retrieve again the genetics of white, black and grey carignan, playing with them, reinterpret their magic, enjoy their game of complicity, try to make a wine that shows the delicacy and depth of some indigenous varieties that are a much of the Empordà DNA, where I dream awake.</p>
        
        <p><strong>Tech sheets</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/2021/A4-encarinyades-2021.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Encarinyades 2021</h3>
            </li>
            <li>
                <a class="pdf-file" href="../../uploads/2022/A4-encarinyades-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Encarinyades 2022</h3>
            </li>
        </ul>
        
    </div>
</div>

<div class="gridder-content" id="desti">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2019/desti-2019-large.jpg" class="media-v" alt="Destí 2019" width="370" height="900" />

        <h2>Destí</h2>
        
        <p><strong>Motivation:</strong> Knowing how to read when life leads you to situations you would never have imagined, and knowing how to get the best out of these because fate has decided this on your life path.</p>
        
        <p><strong>Tech sheets</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/2021/A4-desti-2021.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Destí 2021</h3>
            </li>
            <li>
                <a class="pdf-file" href="../../uploads/2022/A4-desti-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Destí 2022</h3>
            </li>
        </ul>
        
    </div>
</div>



<div class="gridder-content" id="llibertat">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2022/llibertat-2022-large.jpg" class="media-v" alt="Fades del granit 2020" width="370" height="900" />

        <h2>Llibertat</h2>
        
        <p><strong>Motivation:</strong> To pay homage to the red Carignan as one of the best autochthonous varieties of the Empordà, the land that gave Còsmic creative freedom in 2013. Còsmic will always pay tribute to it by making this wine from the heart and freedom.</p>
        
        <p><strong>Tech sheets</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/2022/A4-llibertat-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Llibertat 2022</h3>
            </li>
        </ul>
        
    </div>
</div>



<div class="gridder-content" id="connexio-cosmica">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2019/connexio-cosmica-2019-large.jpg" class="media-v" alt="Connexió Còsmica 2019" width="370" height="900" />

        <h2>Connexió Còsmica</h2>
        
        <p><strong>Motivation</strong>: It is a wine that is born as the result of circumstances. The low valuation of quality agriculture in general and the low prices of grapes, gave birth to this wine, with the intention of dignifying the work of the peasants and closing circles between the farmer and the final consumer. It is a wine that comes from teamwork and the deepest esteem for the Earth, with the aim of dignifying it.</p>
        
        <p><strong>Tech sheets</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/connexio-cosmica-2019-en.pdf" title="Wine sheet in PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Connexió Còsmica 2019</h3>
            </li>
            <li>
                <a class="pdf-file" href="../../uploads/2020/a4-connexio-cosmica-2020.pdf" title="Wine sheet in PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Connexió Còsmica 2020</h3>
            </li>
        </ul>
        
    </div>
</div>

<div class="gridder-content" id="essencia">
    <div class="gridder-copy">
        <img src="../../assets/img/vi-essencia-large.jpg" class="media-v" alt="Essència 2019" width="370" height="900" />

        <h2>Essència</h2>
        
        <p><strong>Motivation: </strong>To worship white carignan in the form of sweet wine, seeking its deepest essence in the absence of water, to enter horizons where sugars and acidity are extreme, and play in a balance of imbalances where the midpoint transports us to the essential.</p>
        
        <p><strong>Tech sheets</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/essencia-2019-es.pdf" title="Wine sheet in PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Essència 2019</h3>
            </li>
        </ul>
        
    </div>
</div>

<div class="gridder-content" id="paciencia-ancestral">
    <div class="gridder-copy">
        <img src="../../assets/img/vi-paciencia-large.jpg" class="media-v" alt="Paciència Ancestral 2017" width="370" height="900" />

        <h2>Paciència Ancestral</h2>
        
        <p><strong>Motivation:</strong> The value of knowing to wait for the right moment, in this wine we want to worship white carignan in a sparkling format, accompany the purity behind ancestral winemaking and be attentive to the dialogue between time and wine, to know how to find the right moment and to be able to toast the fruits of the reward of being patient.</p>
        
        <p><strong>Tech sheets</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/paciencia-ancestral-2017-en.pdf" title="Wine sheet in PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Paciència Ancestral 2017</h3>
            </li>
        </ul>
        
    </div>
</div>

<div class="gridder-content" id="paisatge-cosmic">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2020/paisatge-cosmic-2020-large.jpg" class="media-v" alt="Paisatge Còsmic" width="370" height="900" />

        <h2>Paisatge Còsmic</h2>
        <p><strong>Motivation:</strong> To make a wine that captures the perfume of the environment that surrounds our vineyards with a renewed look, that makes you travel to the landscape of our vineyard through this wine.</p>
        
        <p><strong>Tech sheets</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/2020/a4-paisatge-cosmic-2018.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Consciència Còsmica</h3>
            </li>
        </ul>
    </div>
</div>

<div class="gridder-content" id="vitalitat">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2019/vitalitat-2019-large.jpg" class="media-v" alt="Paciència Ancestral 2017" width="370" height="900" />

        <h2>Vitalitat</h2>
        
        <p><strong>Motivation:</strong> To make a <em style="font-style:italic;">petillant</em>, revitalizing, electric, fresh, festive and sparkling wine, giving value to the Catalan autochthonous varieties with a renewed look.</p>
        
        <p><strong>Tech sheets</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/2021/A4-vitalitat-2021.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Vitalitat 2021</h3>
            </li>
            <li>
                <a class="pdf-file" href="../../uploads/2022/A4-vitalitat-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Vitalitat 2022</h3>
            </li>
        </ul>
        
    </div>
</div>

<div class="gridder-content" id="fades-del-granit">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2020/fades-del-granit-2020-large.jpg" class="media-v" alt="Fades del granit" width="370" height="900" />

        <h2>Fades del granit</h2>
        
        <p><strong>Motivation:</strong> To make a <em style="font-style:italic;">petillant</em>, revitalizing, electric, fresh, festive and sparkling wine, giving value to the Catalan autochthonous varieties with a renewed look.</p>
        
        <p><strong>Tech sheets</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/2021/A4-fades-del-granit-2021.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Fades del granit 2021</h3>
            </li>
            <li>
                <a class="pdf-file" href="../../uploads/2022/A4-fades-del-granit-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Fades del granit 2022</h3>
            </li>
        </ul>
    </div>
</div>






<div class="gridder-content" id="fades-del-calcari">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2022/fades-del-calcari-2022-large.jpg" class="media-v" alt="Fades del granit 2020" width="370" height="900" />

        <h2>Fades del Calcari</h2>
        
        <p><strong>Motivation:</strong> To make a revitalizing, electric, profound, mineral, and sparkling <em style="font-style:italic;">petillant</em> wine. To value the native Catalan varieties of the Penedès with a renewed look.</p>
        
        <p><strong>Tech sheets</strong></p>
        
        <ul class="icons-grid">
            <li>
                <a class="pdf-file" href="../../uploads/2022/A4-fades-del-calcari-2022.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
                <h3>Fades del Calcari 2022</h3>
            </li>
        </ul>
        
    </div>
</div>