<li class="work grid gridder-list" data-griddercontent="#consciencia-cosmica">
    <figure class="effect-bubba bubba3">
        <img src="../assets/img/vins-2020/consciencia-cosmica-2020.jpg" class="media" alt="Consciència Còsmica"/>
        <figcaption>
            <h2><span>Consciència Còsmica</span></h2>
            <a href="#">View more</a>
        </figcaption>			
    </figure>
</li><!-- Remove space between inline-block elements http://codepen.io/chriscoyier/pen/hmlqF

--><li class="work grid gridder-list" data-griddercontent="#constelacio">
    <figure class="effect-bubba bubba3">
        <img src="../assets/img/vins-2020/constelacio-2020.jpg" class="media" alt="Constel·lació"/>
        <figcaption>
            <h2><span>Constel·lació</span></h2>
            <a href="#">View more</a>
        </figcaption>			
    </figure>
</li><!--

--><li class="work grid gridder-list" data-griddercontent="#el-teu-mar">
    <figure class="effect-bubba bubba3">
        <img src="../assets/img/vins-2020/el-teu-mar-2020.jpg" class="media" alt="El teu mar les meves muntanyes"/>
        <figcaption>
            <h2><span>El teu mar les meves muntanyes</span></h2>
            <a href="#">View more</a>
        </figcaption>			
    </figure>
</li><!--

--><li class="work grid gridder-list" data-griddercontent="#gratitud-sauvignon-blanc">
    <figure class="effect-bubba bubba3">
        <img src="../assets/img/vins-2017/gratitud-sauvignon-blanc-2016.jpg" class="media" alt="Gratitud Sauvignon Blanc 2019"/>
        <figcaption>
            <h2><span>Gratitud Blanc</span></h2>
            <a href="#">View more</a>
        </figcaption>			
    </figure>
</li><!--

--><li class="work grid gridder-list" data-griddercontent="#gratitud-cabernet-franc">
    <figure class="effect-bubba bubba3">
        <img src="../assets/img/vins-2017/gratitud-cabernet-franc-2016.jpg" class="media" alt="Gratitud Cabernet franc 2019"/>
        <figcaption>
            <h2><span>Gratitud negre</span></h2>
            <a href="#">View more</a>
        </figcaption>			
    </figure>
</li><!--

--><li class="work grid gridder-list" data-griddercontent="#confianca-garnatxa-roja">
    <figure class="effect-bubba bubba3">
        <img src="../assets/img/vins-2019/confiansa-2019.jpg" class="media" alt="Confiança Garnatxa roja 2019"/>
        <figcaption>
            <h2><span>Confiança</span></h2>
            <a href="#">View more</a>
        </figcaption>			
    </figure>
</li><!--

--><li class="work grid gridder-list" data-griddercontent="#valentia-carinyena-blanca">
    <figure class="effect-bubba bubba3">
        <img src="../assets/img/vins-2019/valentia-2019.jpg" class="media" alt="Valentia Carinyena blanca 2019"/>
        <figcaption>
            <h2><span>Valentia</span></h2>
            <a href="#">View more</a>
        </figcaption>			
    </figure>
</li><!--

--><li class="work grid gridder-list" data-griddercontent="#via-fora-macabeu">
    <figure class="effect-bubba bubba3">
        <img src="../assets/img/vins-2019/via-fora-macabeu-2019.jpg" class="media" alt="Via Fora Macabeu 2019"/>
        <figcaption>
            <h2><span>Via Fora blanc</span></h2>
            <a href="#">View more</a>
        </figcaption>			
    </figure>
</li><!--

--><li class="work grid gridder-list" data-griddercontent="#via-fora-sumoll">
    <figure class="effect-bubba bubba3">
        <img src="../assets/img/vins-2019/via-fora-sumoll-2019.jpg" class="media" alt="Via Fora Sumoll 2019"/>
        <figcaption>
            <h2><span>Via Fora negre</span></h2>
            <a href="#">View more</a>
        </figcaption>			
    </figure>
</li><!--

--><li class="work grid gridder-list" data-griddercontent="#passio-marcelan">
    <figure class="effect-bubba bubba3">
        <img src="../assets/img/vins-2019/passio-2019.jpg" class="media" alt="Passió 2019"/>
        <figcaption>
            <h2><span>Passió</span></h2>
            <a href="#">View more</a>
        </figcaption>			
    </figure>
</li><!--

--><li class="work grid gridder-list" data-griddercontent="#encarinyades">
    <figure class="effect-bubba bubba3">
        <img src="../assets/img/vins-2019/encarinyades-2019.jpg" class="media" alt="Encarinyades 2019"/>
        <figcaption>
            <h2><span>Encarinyades</span></h2>
            <a href="#">View more</a>
        </figcaption>			
    </figure>
</li><!--

--><li class="work grid gridder-list" data-griddercontent="#desti">
    <figure class="effect-bubba bubba3">
        <img src="../assets/img/vins-2019/desti-2019.jpg" class="media" alt="Destí 2019"/>
        <figcaption>
            <h2><span>Destí</span></h2>
            <a href="#">View more</a>
        </figcaption>			
    </figure>
</li><!--


--><li class="work grid gridder-list" data-griddercontent="#llibertat">
    <figure class="effect-bubba bubba3">
        <img src="../assets/img/vins-2022/llibertat-2022.jpg" class="media" alt="Fades del granit 2020"/>
        <figcaption>
            <h2><span>Llibertat</span></h2>
            <a href="#">View more</a>
        </figcaption>			
    </figure>
</li><!--


--><li class="work grid gridder-list" data-griddercontent="#connexio-cosmica">
    <figure class="effect-bubba bubba3">
        <img src="../assets/img/vins-2019/connexio-cosmica-2019.jpg" class="media" alt="Connexió Còsmica 2019"/>
        <figcaption>
            <h2><span>Connexió Còsmica</span></h2>
            <a href="#">View more</a>
        </figcaption>			
    </figure>
</li><!--

--><li class="work grid gridder-list" data-griddercontent="#essencia">
    <figure class="effect-bubba bubba3">
        <img src="../assets/img/vi-essencia.jpg" class="media" alt="Essència 2019"/>
        <figcaption>
            <h2><span>Essència</span></h2>
            <a href="#">View more</a>
        </figcaption>			
    </figure>
</li><!--

--><li class="work grid gridder-list" data-griddercontent="#paciencia-ancestral">
    <figure class="effect-bubba bubba3">
        <img src="../assets/img/vi-paciencia.jpg" class="media" alt="Paciència Ancestral 2017"/>
        <figcaption>
            <h2><span>Paciència Ancestral</span></h2>
            <a href="#">View more</a>
        </figcaption>			
    </figure>
</li><!--

--><li class="work grid gridder-list" data-griddercontent="#paisatge-cosmic">
<figure class="effect-bubba bubba3">
        <img src="../assets/img/vins-2020/paisatge-cosmic-2020.jpg" class="media" alt="Paisatge Còsmic"/>
        <figcaption>
            <h2><span>Paisatge Còsmic</span></h2>
            <a href="#">View more</a>
        </figcaption>			
    </figure>
</li><!--

--><li class="work grid gridder-list" data-griddercontent="#vitalitat">
    <figure class="effect-bubba bubba3">
        <img src="../assets/img/vins-2019/vitalitat-2019.jpg" class="media" alt="Vitalitat 2019"/>
        <figcaption>
            <h2><span>Vitalitat</span></h2>
            <a href="#">View more</a>
        </figcaption>			
    </figure>
</li><!--

--><li class="work grid gridder-list" data-griddercontent="#fades-del-granit">
    <figure class="effect-bubba bubba3">
        <img src="../assets/img/vins-2020/fades-del-granit-2020.jpg" class="media" alt="Fades del granit 2020"/>
        <figcaption>
            <h2><span>Fades del granit</span></h2>
            <a href="#">View more</a>
        </figcaption>			
    </figure>
</li><!--






--><li class="work grid gridder-list" data-griddercontent="#fades-del-calcari">
    <figure class="effect-bubba bubba3">
        <img src="../assets/img/vins-2022/fades-del-calcari-2022.jpg" class="media" alt="Fades del granit 2020"/>
        <figcaption>
            <h2><span>Fades del Calcari</span></h2>
            <a href="#">View more</a>
        </figcaption>			
    </figure>
</li>