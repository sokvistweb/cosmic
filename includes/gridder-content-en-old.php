<div class="gridder-content" id="gratitud-sauvignon-blanc">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2017/gratitud-sauvignon-blanc-2016-large.jpg" class="media-v" alt="Gratitud Sauvignon blanc 2019" width="370" height="900" />

        <h2>Gratitud Sauvignon blanc 2019</h2>
        
        <p><strong>Motivation: </strong>Gratitude to the land and the people with whom I was born and raised, opening to the world integrating the international varieties that best adapt to our land and climate.</p>
        <p><strong>Limited edition: </strong>5,200 bottles</p>
        <p><strong>Grape: </strong>100% sauvignon blanc (eco certified plot)</p>
        <p><strong>Vineyard: </strong>El Coll d’Olivera (Serra del Montmell), Baix Penedès</p>
        <p><strong>Height: </strong>550 m</p>
        <p><strong>Terrain: </strong>very compact limestone red clay</p>
        <p><strong>Winemaking: </strong>grapes harvested by hand with boxes of 10 kg. Maceration with skins for 2 days, smooth draining of the must without pressing.</p>
        <p><strong>Fermentation: </strong>spontaneous 70%, selected 30%</p>
        <p><strong>Fermentation container: </strong>10% amphora, 20% chestnut and 70% stainless</p>
        <p><strong>Total sulphites: </strong>28 mg/l</p>
        <p><strong>Alcohol: </strong>12.5%</p>
        <p><strong>Unfiltered wine</strong></p>
        
        <a class="pdf-file" href="../../uploads/gratitud-sauvignon-blanc-2019-en.pdf" title="Wine sheet in PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>

<div class="gridder-content" id="gratitud-cabernet-franc">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2017/gratitud-cabernet-franc-2016-large.jpg" class="media-v" alt="Gratitud Cabernet franc 2019" width="370" height="900" />

        <h2>Gratitud Cabernet franc 2019</h2>
        
        <p><strong>Motivation: </strong>Gratitude to the land and the people with whom I was born and raised, opening to the world integrating the international varieties that best adapt to our land and climate.</p>
        <p><strong>Limited edition: </strong>4,690 bottles</p>
        <p><strong>Grape: </strong>85% cabernet franc, 15% parellada (eco certified plot)</p>
        <p><strong>Vineyard: </strong>Parcel·la La Solana (Serra del Montmell), Baix Penedès</p>
        <p><strong>Height: </strong>500 m</p>
        <p><strong>Terrain: </strong>very compact limestone red clay</p>
        <p><strong>Winemaking: </strong>manual harvest in boxes of 15 kg. Maceration with skin and scrapes for 7 days, subsequent bleeding and fermentation 50% in amphora and 50% in stainless steel.</p>
        <p><strong>Fermentation: </strong>spontaneous (autochthonous yeast)</p>
        <p><strong>Aging: </strong>tuning in chestnut boots for 2.5 months</p>
        <p><strong>Fermentation container: </strong>70% amphora, 30% stainless</p>
        <p><strong>Unfiltered, total sulphites: </strong>8 mg/l</p>
        <p><strong>Alcoholic grade: </strong>12.5%</p>
        
        <a class="pdf-file" href="../../uploads/gratitud-cabernet-franc-2019-en.pdf" title="Wine sheet in PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>

<div class="gridder-content" id="confianca-garnatxa-roja">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2019/confiansa-2019-large.jpg" class="media-v" alt="Confiança Garnatxa roja 2016" width="370" height="900" />

        <h2>Confiança 2019</h2>
        
        <p><strong>Motivation: </strong>worshiping the Red Grenache, the first step on the seasonal path of making wine, the first interpretation of a whole year of work on earth embracing the value of trust, and jumping into the void of the unknown year after year to keep flying in its sky, where the color is the pink of the sun that sets to be reborn.</p>
        <p><strong>Limited edition: </strong>1,995 bottles</p>
        <p><strong>Grape:</strong> 100% Red Grenache</p>
        <p><strong>Agriculture: </strong>organic with biodynamic criteria</p>
        <p><strong>Vineyard: </strong>50-year-old bush vines</p>
        <p><strong>Places: </strong>Prat d’Egna, Agullana (Alt Empordà)</p>
        <p><strong>Height:</strong> 290 m.</p>
        <p><strong>Terrain:</strong> sandy granite</p>
        <p><strong>Winemaking: </strong>manual harvest in boxes of 15 kg -maceration with skins for 3 days, very soft pressing, fermentation 40% amphora, 60% stainless.</p>
        <p><strong>Fermentation:</strong> spontaneous (autochthonous yeast)</p>
        <p><strong>Aging:</strong> stainless deposit on fine lees</p>
        <p><strong>Fermentation container:</strong> stainless, amphora</p>
        <p><strong>Unfiltered, total sulphites:</strong> 8 mg / l.</p>
        <p><strong>Alcoholic grade:</strong> 12%</p>

        <a class="pdf-file" href="../../uploads/confiansa-2019-en.pdf" title="Fitxa del vi en PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>

<div class="gridder-content" id="valentia-carinyena-blanca">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2019/valentia-2019-large.jpg" class="media-v" alt="Valentia Carinyena blanca 2019" width="370" height="900" />

        <h2>Valentia Carinyena blanca 2019</h2>

        <p><strong>Motivation:</strong> Worship the white cariñena, overlap instinct and intuition with the greatest force and value before the established conditions that surround us and that limit us, to be able to go deep into what we are destined to be, our real identity, the connection with our land and our being, without fear.</p>
        <p><strong>Limited edition: </strong>2,590 bottles</p>
        <p><strong>Grape: </strong>100% white carignan</p>
        <p><strong>Agriculture: </strong>organic with biodynamic criteria</p>
        <p><strong>Vineyard: </strong>in a 60-year-old bush vines</p>
        <p><strong>Places: </strong>Prat d’Egna - l’Estrada, Agullana (Alt Empordà)</p>
        <p><strong>Height: </strong>290m</p>
        <p><strong>Terrain: </strong>sandy granite</p>
        <p><strong>Winemaking: </strong>manual harvest in boxes of 15 kg. Maceration with skins for 4 days, soft pressing, spontaneous fermentation 60% stainless, 20% amphora and 20% chestnut.</p>
        <p><strong>Fermentation: </strong>spontaneous (autochthonous yeast)</p>
        <p><strong>Aging: </strong>50% stainless on fine lees, 50% chestnut boot 3 months</p>
        <p><strong>Fermentation container: </strong>stainless, amphora, chestnut boot</p>
        <p><strong>Unfiltered, total sulphites: </strong>8mg/l</p>
        <p><strong>Alcoholic grade: </strong>12%</p>

        <a class="pdf-file" href="../../uploads/valentia-2019-en.pdf" title="Wine sheet in PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>

<div class="gridder-content" id="via-fora-macabeu">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2019/via-fora-macabeu-2019-large.jpg" class="media-v" alt="Via Fora Macabeu 2019" width="370" height="900" />

        <h2>Via Fora macabeu 2019</h2>
        
        <p><strong>Motivation:</strong> It is a wine that is born from firmness in the difficult situations of life, where our identity can be reduced to oblivion. It is a tribute to wise farmers, who have been firm conservators of our cultural and wine heritage throughout the territory of our beloved country.</p>
        <p><strong>Limited edition: </strong>2,000 bottles</p>
        <p><strong>Grape: </strong>100% macabeu</p>
        <p><strong>Agriculture: </strong>organic with certification</p>
        <p><strong>Vineyard: </strong>in a 75-year-old bush vines</p>
        <p><strong>Place: </strong>El Pauet - Rodonyà - Sierra de Montmell (Alt Camp)</p>
        <p><strong>Terrain: </strong>limestone white clay slopes</p>
        <p><strong>Winemaking: </strong>harvested by hand with 15 kg boxes. Maceration with skins and part of the scrape for 5 days, smooth draining without pressing, spontaneous fermentation in stainless steel tank.</p>
        <p><strong>Fermentation: </strong>native yeast</p>
        <p><strong>Fermentation vessel: </strong>stainless tank</p>
        <p><strong>Aging: </strong>on fine lees in stainless steel tank</p>
        <p><strong>Unfiltered, not clarified, total sulphites: </strong>&lt;10 mg/l</p>
        <p><strong>Alcoholic grade: </strong>11,5%</p>
        
        <a class="pdf-file" href="../../uploads/via-fora-macabeu-2019-en.pdf" title="Wine sheet in PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>

<div class="gridder-content" id="via-fora-sumoll">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2019/via-fora-sumoll-2019-large.jpg" class="media-v" alt="Via Fora Sumoll 2019" width="370" height="900" />

        <h2>Via Fora Sumoll 2019</h2>
        
        <p><strong>Motivation:</strong> It is a wine that is born from firmness in the difficult situations of life, where our identity can be reduced to oblivion. It is a tribute to wise farmers, who have been firm conservators of our cultural and wine heritage throughout the territory of our beloved country.</p>
        <p><strong>Limited edition: </strong>1,290 bottles</p>
        <p><strong>Grape: </strong>100% sumoll</p>
        <p><strong>Agriculture: </strong>organic with biodynamic criteria</p>
        <p><strong>Vineyard: </strong>vineyard in 56-year-old bush vines</p>
        <p><strong>Place: </strong>El Cup d'en Benet, Serra del Montmell (Baix Penedès)</p>
        <p><strong>Terrain: </strong>shallow calcareous clay</p>
        <p><strong>Winemaking: </strong>harvested by hand with 15 kg boxes. Maceration of whole grapes for 7 days, (carbonic maceration) gently pressed, spontaneous fermentation in a 70% stainless tank and 30% amphora.</p>
        <p><strong>Fermentation: </strong>native yeast</p>
        <p><strong>Fermentation vessel: </strong>stainless tank, amphora</p>
        <p><strong>Aging: </strong>in chestnut barrels for 4 months</p>
        <p><strong>Unfiltered, not clarified, total sulphites: </strong>&lt;10 mg/l</p>
        <p><strong>Alcoholic grade:</strong> 12%</p>
        
        <a class="pdf-file" href="../../uploads/via-fora-sumoll-2019-en.pdf" title="Wine sheet in PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>

<div class="gridder-content" id="passio-marcelan">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2019/passio-2019-large.jpg" class="media-v" alt="Passió 2019" width="370" height="900" />

        <h2>Passió 2019</h2>
        
        <p><strong>Motivation:</strong> Passió is the wine that was born as a result of the feeling of love for the vineyard and the lands that I work and that dominate my will and my reason, it is the link between all the wines I make, the motor value among all them and it is also a hug of complicity between the two territories that I interpret with absolute passion, my origins and my present.</p>
        <p><strong>Limited edition: </strong>2,702</p>
        <p><strong>Grape: </strong>80% marselan, 20% black carignan</p>
        <p><strong>Agriculture: </strong>organic with biodynamic criteria</p>
        <p><strong>Vineyard: </strong>20 years vine formation, 6 years bush vines</p>
        <p><strong>Places: </strong>Serra del Montmell (Baix Penedès), Prat d’Egna Agullana (Alt Empordà)</p>
        <p><strong>Height: </strong>500m, 280m</p>
        <p><strong>Terrain: </strong>calcareous clay, sandy granite</p>
        <p><strong>Winemaking: </strong>manual harvest in boxes of 15 kg. Whole grape maceration without skidding or stepping (carbonic maceration) for 9 days, pressing and spontaneous fermentation in 30% amphorae and 70% stainless steel tank.</p>
        <p><strong>Fermentation: </strong>spontaneous (autochthonous yeast)</p>
        <p><strong>Aging: </strong>50% chestnut boot, 50% stainless on fine lees</p>
        <p><strong>Fermentation container: </strong>stainless, amphora</p>
        <p><strong>Unfiltered, total sulphites: </strong>8mg/l</p>
        <p><strong>Alcoholic degree: </strong>12%</p>
        
        <a class="pdf-file" href="../../uploads/passio-2019-en.pdf" title="Wine sheet in PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>

<div class="gridder-content" id="llibertat">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2019/encarinyades-2019-large.jpg" class="media-v" alt="Encarinyades 2019" width="370" height="900" />

        <h2>Encarinyades 2019</h2>
        
        <p><strong>Motivation: </strong>Retrieve again the genetics of white, black and grey carignan, playing with them, reinterpret their magic, enjoy their game of complicity, try to make a wine that shows the delicacy and depth of some indigenous varieties that are a much of the Empordà DNA, where I dream awake.</p>
        <p><strong>Limited edition: </strong>3,750 bottles</p>
        <p><strong>Grape: </strong>80% carignan white, 15% carignan black and 5% carignan grey (certified organic plot)</p>
        <p><strong>Vineyard: </strong>bush vines of 6 years, 60 years, 20 years</p>
        <p><strong>Place: </strong>Prat d’Egna, Agullana (Alt Empordà)</p>
        <p><strong>Height: </strong>300m</p>
        <p><strong>Terrain: </strong>sandy granite</p>
        <p><strong>Winemaking: </strong>manual harvest in 15kg boxes. Maceration with skin and skin for 5 days, pressing and fermentation. 100% in clay amphora.</p>
        <p><strong>Fermentation: </strong>spontaneous (autochthonous yeast)</p>
        <p><strong>Aging: </strong>70% stainless steel deposit, 30% chestnut boot for 2.5 months</p>
        <p><strong>Fermentation container: </strong>100% amphora</p>
        <p><strong>Unfiltered, total sulfites: </strong>8 mg/l</p>
        <p><strong>Alcoholic grade: </strong>12%</p>
        
        <a class="pdf-file" href="../../uploads/encarinyades-2019-en.pdf" title="Wine sheet in PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>

<div class="gridder-content" id="desti">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2019/desti-2019-large.jpg" class="media-v" alt="Destí 2019" width="370" height="900" />

        <h2>Destí 2019</h2>
        
        <p><strong>Motivation:</strong> Knowing how to read when life leads you to situations you would never have imagined, and knowing how to get the best out of these because fate has decided this on your life path.</p>
        <p><strong>Limited edition: </strong>5,350 bottles</p>
        <p><strong>Grape:</strong> 100% Muscat of Alexandria (eco certified plot)</p>
        <p><strong>Vineyard: </strong>La vinya del Joan, Capmany (Alt Empordà)</p>
        <p><strong>Height: </strong>122m.</p>
        <p><strong>Terrain: </strong>sandy granite</p>
        <p><strong>Vinification: </strong>manual harvest in 15kg boxes. Maceration with whole grape for 3 weeks.</p>
        <p><strong>Fermentation: </strong>spontaneous (autochthonous yeast)</p>
        <p><strong>Aging: </strong>on fine lees in stainless steel tank</p>
        <p><strong>Fermentation container: </strong>stainless</p>
        <p><strong>Unfiltered, total sulfites: </strong>8mg/l.</p>
        <p><strong>Alcoholic grade: </strong>11,5%</p>
        
        <a class="pdf-file" href="../../uploads/desti-2019-en.pdf" title="Wine sheet in PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>

<div class="gridder-content" id="connexio-cosmica">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2019/connexio-cosmica-2019-large.jpg" class="media-v" alt="Connexió Còsmica 2019" width="370" height="900" />

        <h2>Connexió Còsmica 2019</h2>
        
        <p><strong>Motivation</strong>: It is a wine that is born as the result of circumstances. The low valuation of quality agriculture in general and the low prices of grapes, gave birth to this wine, with the intention of dignifying the work of the peasants and closing circles between the farmer and the final consumer. It is a wine that comes from teamwork and the deepest esteem for the Earth, with the aim of dignifying it.</p>
        <p><strong>Limited edition: </strong>9,400 bottles</p>
        <p><strong>Grape:</strong> 85% Xarel·lo, 10% Chardonnay and 5% Muscat of Alexandria (certified organic plot)</p>
        <p><strong>Places: </strong>Camp del Boter (Rodonyà), Serra del Montmell, les Parellades (Penedès)</p>
        <p><strong>Heights: </strong>300 and 500m.</p>
        <p><strong>Terrain: </strong>calcareous clay</p>
        <p><strong>Winemaking: </strong>manual harvest. Maceration with skin and part of scraping for 8 days, pressed and fermented in stainless steel tank.</p>
        <p><strong>Fermentation: </strong>spontaneous with native yeast</p>
        <p><strong>Aging: </strong>on fine lees in stainless steel tank</p>
        <p><strong>Unfiltered, total sulphites: </strong>10mg/l.</p>
        <p><strong>Alcoholic grade: </strong>12%</p>
        
        <a class="pdf-file" href="../../uploads/connexio-cosmica-2019-en.pdf" title="Wine sheet in PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>

<div class="gridder-content" id="essencia">
    <div class="gridder-copy">
        <img src="../../assets/img/vi-essencia-large.jpg" class="media-v" alt="Essència 2019" width="370" height="900" />

        <h2>Essència 2019</h2>
        
        <p><strong>Motivation: </strong>To worship white carignan in the form of sweet wine, seeking its deepest essence in the absence of water, to enter horizons where sugars and acidity are extreme, and play in a balance of imbalances where the midpoint transports us to the essential.</p>
        <p><strong>Limited edition: </strong>1,390 bottles</p>
        <p><strong>Grape: </strong>100% white carignan</p>
        <p><strong>Agriculture: </strong>organic with biodynamic criteria</p>
        <p><strong>Vineyard: </strong>in a 60-year-old bush vines</p>
        <p><strong>Places: </strong>Prat d’Egna - L’Estrada, Agullana (Alt Empordà)</p>
        <p><strong>Height: </strong>290 m</p>
        <p><strong>Terrain: </strong>sandy granite</p>
        <p><strong>Winemaking: </strong>early harvest, looking for a low sugar content and a high acidity content. Maceration with skins for 4 days, pressed, and reduction of the wort by very slow boiling, where we will reduce the original volume by 70%, concentrating sugars and acidity. Subsequent cooling and replanting with original flower wort to start a smooth and slow fermentation that will last many months until its end.</p>
        <p><strong>Fermentation: </strong>spontaneous (autochthonous yeast)</p>
        <p><strong>Aging: </strong>oxidative in open stainless tank and glass demijohns</p>
        <p><strong>Fermentation vessel: </strong>stainless, glass demijohns</p>
        <p><strong>Unfiltered, contains no added sulphites</strong></p>
        <p><strong>Residual sugars: </strong>410 g/liter</p>
        <p><strong>Total acidity: </strong>9.5 ATS</p>
        <p><strong>Alcoholic grade: </strong>9%</p>
        
        <a class="pdf-file" href="../../uploads/essencia-2019-es.pdf" title="Wine sheet in PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>

<div class="gridder-content" id="paciencia-ancestral">
    <div class="gridder-copy">
        <img src="../../assets/img/vi-paciencia-large.jpg" class="media-v" alt="Paciència Ancestral 2017" width="370" height="900" />

        <h2>Paciència Ancestral 2017</h2>
        
        <p><strong>Motivation:</strong> The value of knowing to wait for the right moment, in this wine we want to worship white carignan in a sparkling format, accompany the purity behind ancestral winemaking and be attentive to the dialogue between time and wine, to know how to find the right moment and to be able to toast the fruits of the reward of being patient.</p>
        <p><strong>Limited Edition: </strong>2,000 bottles</p>
        <p><strong>Grape: </strong>100% white carignan</p>
        <p><strong>Agriculture: </strong>organic with biodynamic criteria</p>
        <p><strong>Vineyard: </strong>in a 60-year-old bush vines</p>
        <p><strong>Places: </strong>Prat d’Egna, Agullana (Alt Empordà)</p>
        <p><strong>Height: </strong>290 m</p>
        <p><strong>Terrain: </strong>sandy granite</p>
        <p><strong>Winemaking: </strong>manual harvest in boxes of 15 kg. Maceration with skins for 2 days, smooth draining without pressing, spontaneous fermentation in clay amphorae with end of fermentation and aging in bottle, rinsing in manual desk, disgorgement by hand and corked with subsequent rest until consumption.</p>
        <p><strong>Fermentation: </strong>spontaneous (autochthonous yeast)</p>
        <p><strong>Aging: </strong>24 months in bottle</p>
        <p><strong>Fermentation container: </strong>amphora, glass bottle</p>
        <p><strong>Unfiltered, contains no added sulphites</strong></p>
        <p><strong>Alcoholic degree: </strong>12%</p>
        
        <a class="pdf-file" href="../../uploads/paciencia-ancestral-2017-en.pdf" title="Wine sheet in PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>

<div class="gridder-content" id="vitalitat">
    <div class="gridder-copy">
        <img src="../../assets/img/vins-2019/vitalitat-2019-large.jpg" class="media-v" alt="Paciència Ancestral 2017" width="370" height="900" />

        <h2>Vitalitat 2019</h2>
        
        <p><strong>Motivation:</strong> To make a <em>petillant</em>, revitalizing, electric, fresh, festive and sparkling wine, giving value to the Catalan autochthonous varieties with a renewed look.</p>
        <p><strong>Limited edition: </strong>3,100 bottles</p>
        <p><strong>Grape: </strong>92% parellada, 8% muscat (eco certified plot)</p>
        <p><strong>Vineyard: </strong>50 years and 15 years</p>
        <p><strong>Place: </strong>Serra de Montmell (Baix Penedès)</p>
        <p><strong>Height: </strong>500m</p>
        <p><strong>Terrain: </strong>calcareous clay</p>
        <p><strong>Winemaking: </strong>manual harvest in boxes of 15 kg. Skimming and maceration with skins for 3 days, smooth draining of the must without pressing.</p>
        <p><strong>Fermentation: </strong>spontaneous (autochthonous yeast) with end of fermentation in the bottle</p>
        <p><strong>Aging: </strong>on fine lees to the bottle until the moment of consumption</p>
        <p><strong>Fermentation container: </strong>stainless and glass bottle</p>
        <p><strong>Unfiltered, total sulphites: </strong>8 mg/l</p>
        <p><strong>Alcoholic grade: </strong>11%</p>
        
        <a class="pdf-file" href="../../uploads/vitalitat-2019-en.pdf" title="Wine sheet in PDF" target="_blank"><img src="../../assets/img/pdf-icon.svg" alt="PDF icon" width="56" height="72"></a>
    </div>
</div>