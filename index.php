<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" lang="ca"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" lang="ca"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" lang="ca"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" lang="ca"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" lang="ca"><!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Còsmic Vinyaters</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="Còsmic vinyaters és un microceller que elabora vins naturals i vins ecològics. Fem vins culturals per donar plaer al món." />
    <meta name="keywords" content="vins, celler, ecològic" />
    <meta name="author" content="Sokvist" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="apple-touch-icon" href="apple-touch-icon.png" /><!-- 57×57px -->
    <link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png"><!-- 180×180px -->
    <link rel="stylesheet" href="assets/css/style.css">
	<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,600,900' rel='stylesheet' type='text/css'>
    <link rel="canonical" href="http://cosmic.cat/">
    <link hreflang="es" href="http://cosmic.cat/es/" rel="alternate">
    <link hreflang="en" href="http://cosmic.cat/en/" rel="alternate">

    <script src="assets/js/vendor/modernizr.js"></script>
  	
</head>
<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status" class="spinner">
            <div id="loading">
            <div id="loading-center">
            <div id="loading-center-absolute">
            <div class="object" id="object_one"></div>
            <div class="object" id="object_two"></div>
            <div class="object" id="object_three"></div>
            <div class="object" id="object_four"></div>
            <div class="object" id="object_five"></div>
            <div class="object" id="object_six"></div>
            <div class="object" id="object_seven"></div>
            <div class="object" id="object_eight"></div>
            <div class="object" id="object_big"></div>
            </div>
            </div>
            </div>
        </div>
    </div> <!-- /#preloader -->
    
    <div class="eupopup eupopup-top"></div>

	<header class="cd-header">
		<div id="cd-logo"><a href="http://cosmic.cat/" rel="home" title="Còsmic Vinyaters"><img src="assets/img/cosmic-logo.svg" alt="Còsmic"></a></div>

		<nav class="cd-main-nav">
			<ul class="menu" id="nav">
				<li class="hidden"><a href="#la-vinya">La Vinya</a></li>
				<li><a href="#el-celler">El Celler</a></li>
				<li><a href="#visites">Visites</a></li>
                <li><a href="#punts-de-venda">Punts de venda</a></li>
				<li class="hidden"><a href="#contacte">Contacte</a></li>
                <li><a href="/els-vins/">Els Vins</a></li>
			</ul>
		</nav> <!-- cd-main-nav -->
        
        <ul class="language-chooser">
            <li class="lang-es active"><a href="es" hreflang="es" title="Español"><span>es</span></a></li>
            <li class="lang-en"><a href="en" hreflang="en" title="English"><span>en</span></a></li>
        </ul>
	</header>

	<main class="cd-main-content">
		<div class="cd-fixed-bg cd-bg-1-<?php echo(rand(1,3)); ?>" id="la-vinya">
			<h1><img src="assets/img/cosmic-vinyaters.svg" alt="Còsmic vinyaters"></h1>
		</div> <!-- cd-fixed-bg -->

		<div class="cd-scrolling-bg cd-color-2 with-img">
			<div class="cd-container">
                <div class="spotlight">
                    <div class="slider image">
                        <?php include ("includes/slider-lavinya.php"); ?>
                    </div>
                    <div class="content copy-right">
                        <p>Còsmic vinyaters és <strong>una filosofia de cuidar vinyes i fer vi</strong> basada en els valors de la confiança en el que sentim, la valentia en el que som, la passió en el que fem, l’essència en el buscar, la paciència en el trobar, amb gratitud en el rebre per a ser més lliures d’esperit i més conscients en el nostre camí creatiu, com a pagesos i vinaters que som.</p>
                        
                        <p>La mirada de Còsmic es centra en <strong>dos paisatges molt diferents  de la geografia catalana</strong>, però molt nostres: <strong>Agullana, Capmany i La Vajol</strong> a l’Alt Empordà, on donem culte a varietats autòctones  empordaneses, i <strong>Rodonyà i la Serra del Montmell</strong> al Baix Penedès, on cultivem varietats autòctones penedesenques juntament amb varietats foranes ben adaptades al nostre terrer.</p>
                        <div class="arrow bounce">
                            <a href="#content-2" class="more"><span>Seguir llegint</span></a>
                        </div>
                    </div>
                </div>
			</div> <!-- cd-container -->
		</div> <!-- cd-scrolling-bg -->

		<div class="cd-fixed-bg cd-bg-2" id="el-celler">
			<h2>El Celler</h2>
		</div> <!-- cd-fixed-bg -->

		<div class="cd-scrolling-bg cd-color-3 with-img" id="content-2">
			<div class="cd-container">
                <div class="spotlight reverse">
                    <div class="slider image">
                        <?php include ("includes/slider-elceller.php"); ?>
                    </div>
                    <div class="content copy-left">
                        <p>Dos mons units sota una mateixa mirada que desxifra terrers, estima raïms, interpreta varietats, toca la terra i actua treballant 9,5 ha de vinya, cultivades de la manera més natural possible practicant una <strong>agricultura ecològica i biodinàmica</strong>.</p>
                        
                        <p>El nostre camí creatiu es centra en fer <strong>vins integrals i purs</strong> sempre fets d’una sola varietat de raïm (puresa sobre puresa) amb una mínima intervenció i un màxim respecte a l’origen natural i energètic de la varietat de raïm, la terra, el clima i l’esperit de la persona que hi ha darrera, que fa ús de tècniques de <strong>neteja energètica, geometria sagrada i vibracions sonores</strong> per a l’harmonització de l’espai, les persones i els vins que s’hi conceben.</p>
                    </div>
                </div>
			</div> <!-- cd-container -->
		</div> <!-- cd-scrolling-bg -->


		<div class="cd-fixed-bg cd-bg-4" id="visites">
			<h2>Visites</h2>
		</div> <!-- cd-fixed-bg -->
        
        <div class="cd-scrolling-bg cd-color-2 with-img">
			<div class="cd-container">
                <div class="spotlight">
                    <div class="slider image">
                        <?php include ("includes/slider-visites.php"); ?>
                    </div>
                    <div class="content">
                        <p>Depenent de l’època de l’any, s’organitzen visites guiades a les nostres vinyes amb tast final al celler per a grups de com a mínim 6 persones i màxim de 10 persones, sempre amb reserva prèvia al correu <a href="mailto:info@cosmic.cat">info@cosmic.cat</a>.</p>
                        
                        <p>La durada aproximada de les visites guiades és entre 1,5 i 2 hores. Durant el recorregut es coneixen les vinyes i la filosofia de treball del celler, i la visita acaba amb un tast final de tots els vins que elaborem.</p>
                        
                        <p>El cost de la visita és de 15 euros per persona.</p>
                    </div>
                </div>
			</div> <!-- cd-container -->
		</div> <!-- cd-scrolling-bg -->
        
        <div class="cd-fixed-bg cd-bg-5" id="punts-de-venda">
			<h2>Punts de venda</h2>
		</div> <!-- cd-fixed-bg -->
        
        <div class="cd-scrolling-bg cd-color-3">
			<div class="cd-container">
				<p>Pots adquirir els nostres vins adreçant-te a qualsevol dels nostres distribuïdors provincials. També fem venda directa des del celler, únicament a particulars. Contacta'ns via whatts al <a aria-label="Chat on WhatsApp" href="https://wa.me/639338176">639 338 176</a> per a concretar dia i hora per venir a comprar el teu vi.</p>
                
                <h3>Catalunya</h3>
                
                <h4>Girona:</h4>
                <ul>
                    <li>Jaume Jordà, <a href="tel:0034629766722">629 766 722</a> - <a href="mailto:jaume@jaumejorda.com">jaume@jaumejorda.com</a></li>
                </ul>
                
                <h4>Barcelona:</h4>
                <ul>
                    <li>Vilaviniteca, <a href="tel:902327777">902 327 777</a> - <a href="mailto:info@vilaviniteca.es">info@vilaviniteca.es</a></li>
                    <li>Jaume Jordà, <a href="tel:0034629766722">629 766 722</a> - <a href="mailto:jaume@jaumejorda.com">jaume@jaumejorda.com</a></li>
                </ul>
                
                <h4>Tarragona:</h4>
                <ul>
                    <li>Bonbocam, <a href="tel:0034675900215">675 900 215</a> - <a href="mailto:bonbocam@bonbocam.com">bonbocam@bonbocam.com</a></li>
                </ul>
                
                <h3>Espanya</h3>
                <ul>
                    <li>Marbella (Costa del Sol): Just naturally wines, <a href="627331450">627 331 450</a> - <a href="mailto:info@justnaturallywine.com">info@justnaturallywine.com</a>  </li>
                    <li>Eivissa: Supernaturalwines, <a href="635098568">635 098 568</a> - <a href="mailto:orders@supernaturalwines.es">orders@supernaturalwines.es</a></li>
                    <li>Alacant: Winemultiverse, <a href="690630245">690 630 245</a> - <a href="mailto:winemultiverse@gmail.com">winemultiverse@gmail.com</a></li>
                    <li>Euskadi: Manso hermanos, <a href="630097140">630 097 140</a> - <a href="mailto:manso@mansohermanos.com">manso@mansohermanos.com</a></li>
                </ul>
                
                <h3>Internacional</h3>
                <p>Consulteu directament al celler: <a href="tel:0034639338176">+34 639 338 176</a> - <a href="mailto:info@cosmic.cat">info@cosmic.cat</a></p>
                
			</div> <!-- cd-container -->
		</div> <!-- cd-scrolling-bg -->
        
        <div class="cd-fixed-bg cd-bg-6" id="contacte">
			<h2>Contacte</h2>
		</div> <!-- cd-fixed-bg -->
        
        <div class="cd-scrolling-bg cd-color-1">
			<div class="cd-container">
                <dl>
                    <dt>Celler Còsmic Vinyaters</dt>
                    <dd>Plaça Maria Teresa Pallejà, 3</dd>
                    <dd>17707 Agullana (Alt Empordà)</dd>
                    <dd>Tel. <a href="tel:0034639338176">+ 34 639 338 176</a></dd>
                    <dd><a href="mailto:info@cosmic.cat">info@cosmic.cat</a></dd>
                </dl>
                
                <p class="location"><a href="https://www.google.es/maps/place/Pla%C3%A7a+Maria+Teresa+de+Palleja,+3,+17707+Agullana,+Girona/@42.39429,2.845546,17z/data=!3m1!4b1!4m2!3m1!1s0x12ba9983354ad99d:0xf5cfb6da06874e1a" title="Obre la pàgina de Google Maps" target="_blank">Veure a Google Maps</a></p>
                
                <ul class="social">
                    <li><a class="youtube" href="https://youtu.be/Lqs9nZN1gvs" title="Còsmic a YouTube" target="_blank"></a></li>
                    <li><a class="facebook" href="https://www.facebook.com/cosmicceller.cosmicceller" title="La nostra pàgina de Facebook" target="_blank"></a></li>
                    <li><a class="twitter" href="https://twitter.com/CosmicCeller" title="Visita'ns a Twitter" target="_blank"></a></li>
                    <li><a class="insta" href="https://www.instagram.com/cosmicceller/" title="Visita'ns a Instagram" target="_blank"></a></li>
                </ul>

			</div> <!-- cd-container -->
		</div> <!-- cd-scrolling-bg -->
                       
        <div class="eupopup-container eupopup-container-bottomright"> 
            <div class="eupopup-markup">
                <div class="eupopup-head">Aquesta web fa servir cookies</div> 
                <div class="eupopup-body">Utilitzem cookies per assegurar-nos que us donem la millor experiència al nostre lloc web. Si continua navegant, considerem que accepta el seu ús.</div> 
                <div class="eupopup-buttons"> 
                    <a href="#" class="eupopup-button eupopup-button_1">Acceptar</a> 
                    <a href="avis-legal/politica-de-cookies/" target="_blank" class="eupopup-button eupopup-button_2">Més info</a>
                </div> 
                <div class="clearfix"></div> 
                <a href="#" class="eupopup-closebutton">
                    <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"/><path d="M0 0h24v24h-24z" fill="none"/></svg>
                </a> 
            </div> 
        </div>
                        
        <a href="#0" class="cd-top">Top</a>
                        
        <div class="footer">
            <div class="container">
                <span><a href="avis-legal/" title="Avís legal">Avís legal</a></span> · <span><a href="avis-legal/politica-de-cookies/" title="Política de cookies">Política de cookies</a></span> · <span><a href="avis-legal/politica-de-privacitat/" title="Política de privacitat">Política de privacitat</a></span> | <span>© Còsmic Vinyaters</span> | <span><a href="http://sokvist.com" title="Sokvist, Web &amp; SEO">Sokvist, Web &amp; SEO</a></span>
            </div>
        </div>
	
    </main> <!-- cd-main-content -->
                    
                        
    <script src="assets/js/vendor/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js/plugins.min.js"></script>
    <script src="assets/js/js/main.min.js"></script>
                        
    <!-- Preloader -->
    <script type="text/javascript">
        //<![CDATA[
		$(window).load(function() { // makes sure the whole site is loaded
			$('#status').fadeOut(); // will first fade out the loading animation
			$('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
		})
		//]]>
    </script>
    
</body>
</html>